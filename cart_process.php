<?php

session_start(); //start session
include_once("config.inc.php"); //include config file
include 'db.php';
setlocale(LC_MONETARY, "en_US"); // US national format (see : http://php.net/money_format)
############# add products to session #########################
if (isset($_POST["add"])) {

    foreach ($_POST as $key => $value) {
        $post[$key] = filter_var($value, FILTER_SANITIZE_STRING); //create a new product array 
    }



    //we need to get product name and price from database.
//	$statement = $mysqli_conn->prepare("SELECT product_name, product_price FROM products_list WHERE product_code=? LIMIT 1");
//	$statement->bind_param('s', $new_product['product_code']);
//	$statement->execute();
//	$statement->bind_result($product_name, $product_price);

    $db->where('id', $post['product_menu']);
    $menuItem = $db->getOne('tbl_resto_menu');

    $db->where('id', $post['product_code']);
    $item = $db->getOne('tbl_resto_item');

    $db->where('step', $post['product_step']);
    $stepsItem = $db->get('tbl_resto_item');

    if ($post['product_type'] == "ready") {
        if (!isset($_SESSION["products"][$post['product_type']][$post['product_code']]["product_ingridient"][$post['product_ingridient']]['qty'])) {
            $_SESSION["products"][$post['product_type']][$post['product_code']]["product_ingridient"][$post['product_ingridient']]['qty'] = 0;
        }
        $_SESSION["products"][$post['product_type']][$post['product_code']]["product_name"] = $item["name"]; //fetch product name from database
        $_SESSION["products"][$post['product_type']][$post['product_code']]["product_menu"] = $menuItem["name"]; //fetch product name from database
        $_SESSION["products"][$post['product_type']][$post['product_code']]["product_ingridient"][$post['product_ingridient']]['name'] = $post['product_ingridient'];
        $_SESSION["products"][$post['product_type']][$post['product_code']]["product_ingridient"][$post['product_ingridient']]['price'] = $post['product_price'];
        $_SESSION["products"][$post['product_type']][$post['product_code']]["product_ingridient"][$post['product_ingridient']]['qty']++;
    }

    if ($post['product_type'] == "wok") {
        if ($post["product_step"] == 1) {
            /* foreach ($_SESSION as $products) {
              foreach($products['wok'] as $productW=>$productV){
              foreach($productV as $wokK=>$wokV){
              if ($wokV['product_step'] == $post["product_step"]) {
              unset($_SESSION["products"][$post['product_type']]);
              }
              //                            foreach($product['product_ingridient'] as $item=>$value){
              //                                $_SESSION["products"]["total"] += $value['price']*$value['qty'];
              //                            }
              }
              }
              } */
            foreach ($stepsItem as $stepItem) {
                $cWok = $_SESSION["products"]['currentWok'];
                unset($_SESSION["products"][$post['product_type']][$cWok]);
            }

            $wCount = count($_SESSION["products"]["wok"]);
            if (empty($wCount)) {
                $wokCount = 0;
            } else {
                $wokCount = count($_SESSION["products"]["wok"]);
            }

            $_SESSION["products"]['currentWok'] = "Wok-" . $item["name"] . ++$wokCount;

            $currentWok = $_SESSION["products"]['currentWok'];



            //$_SESSION["products"][$post['product_type']][$post['product_code']]["product_name"] = "Wok-".$item["name"]; //fetch product name from database
            $_SESSION["products"][$post['product_type']][$currentWok]['items'][$post['product_code']]["product_name"] = $item["name"]; //fetch product name from database
            $_SESSION["products"][$post['product_type']][$currentWok]['items'][$post['product_code']]["product_step"] = $post["product_step"]; //fetch product name from database
            $_SESSION["products"][$post['product_type']][$currentWok]['items'][$post['product_code']]["product_ingridient"][$post['product_ingridient']]['name'] = $post['product_ingridient'];
            $_SESSION["products"][$post['product_type']][$currentWok]['items'][$post['product_code']]["product_ingridient"][$post['product_ingridient']]['price'] = $post['product_price'];
            $_SESSION["products"][$post['product_type']][$currentWok]['items'][$post['product_code']]["product_ingridient"][$post['product_ingridient']]['qty'] = 1;

            $_SESSION["products"][$post['product_type']][$currentWok]['qty'] = 1;
//$_SESSION["products"] = $_SESSION["products"] + $woks["products"];
        } else if ($post["product_step"] == 2 || $post["product_step"] == 3) {


            $currentWok = $_SESSION["products"]['currentWok'];
            foreach ($stepsItem as $stepItem) {
                unset($_SESSION["products"][$post['product_type']][$currentWok]['items'][$stepItem["id"]]);
            }
            //$_SESSION["products"][$post['product_type']][$post['product_code']]["product_name"] = "Wok-".$item["name"]; //fetch product name from database
            $_SESSION["products"][$post['product_type']][$currentWok]['items'][$post['product_code']]["product_name"] = $item["name"]; //fetch product name from database
            $_SESSION["products"][$post['product_type']][$currentWok]['items'][$post['product_code']]["product_step"] = $post["product_step"]; //fetch product name from database
            $_SESSION["products"][$post['product_type']][$currentWok]['items'][$post['product_code']]["product_ingridient"][$post['product_ingridient']]['name'] = $post['product_ingridient'];
            $_SESSION["products"][$post['product_type']][$currentWok]['items'][$post['product_code']]["product_ingridient"][$post['product_ingridient']]['price'] = $post['product_price'];
            $_SESSION["products"][$post['product_type']][$currentWok]['items'][$post['product_code']]["product_ingridient"][$post['product_ingridient']]['qty'] = 1;

            //$_SESSION["products"] = $_SESSION["products"] + $woks["products"];
        } else if ($post["product_step"] == 4) {


            $currentWok = $_SESSION["products"]['currentWok'];

            if (!isset($_SESSION["products"][$post['product_type']][$currentWok]['items'][$post['product_code']]["product_ingridient"][$post['product_ingridient']]['qty'])) {
                $_SESSION["products"][$post['product_type']][$currentWok]['items'][$post['product_code']]["product_ingridient"][$post['product_ingridient']]['qty'] = 0;
            }

            //$_SESSION["products"][$post['product_type']][$post['product_code']]["product_name"] = "Wok-".$item["name"]; //fetch product name from database
            $_SESSION["products"][$post['product_type']][$currentWok]['items'][$post['product_code']]["product_name"] = $item["name"]; //fetch product name from database
            $_SESSION["products"][$post['product_type']][$currentWok]['items'][$post['product_code']]["product_step"] = $post["product_step"]; //fetch product name from database
            $_SESSION["products"][$post['product_type']][$currentWok]['items'][$post['product_code']]["product_ingridient"][$post['product_ingridient']]['name'] = $post['product_ingridient'];
            $_SESSION["products"][$post['product_type']][$currentWok]['items'][$post['product_code']]["product_ingridient"][$post['product_ingridient']]['price'] = $post['product_price'];
            $_SESSION["products"][$post['product_type']][$currentWok]['items'][$post['product_code']]["product_ingridient"][$post['product_ingridient']]['qty']++;

            //$_SESSION["products"] = $_SESSION["products"] + $woks["products"];
        }
    }
    //
    //while($statement->fetch()){
//        if(isset($_SESSION["products"])){  //if session var already exist
//                if(isset($_SESSION["products"][$new_product['product_code']])) //check item exist in products array
//                {
//                        unset($_SESSION["products"][$new_product['product_code']]); //unset old item
//                }			
//        }
    //$_SESSION["products"][$post['product_code']] = $new_product;	//update products with new item array	
    //}
    $_SESSION["products"]["total"] = "";
    foreach ($_SESSION as $products) {
        foreach ($products['ready'] as $product) {
            foreach ($product['product_ingridient'] as $item => $value) {
                $_SESSION["products"]["total"] += $value['price'] * $value['qty'];
            }
        }
        foreach ($products['wok'] as $productW => $productV) {
            foreach ($productV['items'] as $product) {
                foreach ($product['product_ingridient'] as $item => $value) {
                    $_SESSION["products"]["total"] += ($value['price'] * $value['qty']) * $productV['qty'];
                }
            }
        }
    }

    //$_SESSION["products"] = "";

    die(json_encode($_SESSION)); //output json 
}
############# Modify products to session #########################
if (isset($_POST["qty"])) {

    foreach ($_POST as $key => $value) {
        $post[$key] = filter_var($value, FILTER_SANITIZE_STRING); //create a new product array 
    }

    if ($post['product_type'] == "ready") {

        $productQty = $_SESSION["products"][$post['product_type']][$post['product_code']]["product_ingridient"][$post['product_ingridient']]['qty'];

        if ($post['qty'] == "add") {
            $_SESSION["products"][$post['product_type']][$post['product_code']]["product_ingridient"][$post['product_ingridient']]['qty']++;
        }
        if ($post['qty'] == "remove") {
            if ($productQty > 1) {
                $_SESSION["products"][$post['product_type']][$post['product_code']]["product_ingridient"][$post['product_ingridient']]['qty']--;
            } else {
                unset($_SESSION["products"][$post['product_type']][$post['product_code']]["product_ingridient"][$post['product_ingridient']]);
            }

            if (count($_SESSION["products"][$post['product_type']][$post['product_code']]["product_ingridient"]) < 1) {
                unset($_SESSION["products"][$post['product_type']][$post['product_code']]);
            }
        }
    }

    if ($post['product_type'] == "wok") {
        $currentWok = $_SESSION["products"]['currentWok'];

        $productQty = $_SESSION["products"][$post['product_type']][$currentWok]['items'][$post['product_code']]["product_ingridient"][$post['product_ingridient']]['qty'];

        if ($post['qty'] == "add") {
            $_SESSION["products"][$post['product_type']][$currentWok]['items'][$post['product_code']]["product_ingridient"][$post['product_ingridient']]['qty']++;
        }
        if ($post['qty'] == "remove") {
            if ($productQty > 1) {
                $_SESSION["products"][$post['product_type']][$currentWok]['items'][$post['product_code']]["product_ingridient"][$post['product_ingridient']]['qty']--;
            } else {
                unset($_SESSION["products"][$post['product_type']][$currentWok]['items'][$post['product_code']]);
            }

//                if (count($_SESSION["products"][$post['product_type']][$post['product_code']]["product_ingridient"]) < 1) {
//                    unset($_SESSION["products"][$post['product_type']][$post['product_code']]);
//                }
        }
    }

    $_SESSION["products"]["total"] = "";
    foreach ($_SESSION as $products) {
        foreach ($products['ready'] as $product) {
            foreach ($product['product_ingridient'] as $item => $value) {
                $_SESSION["products"]["total"] += $value['price'] * $value['qty'];
            }
        }
        foreach ($products['wok'] as $productW => $productV) {
            foreach ($productV['items'] as $product) {
                foreach ($product['product_ingridient'] as $item => $value) {
                    $_SESSION["products"]["total"] += ($value['price'] * $value['qty']) * $productV['qty'];
                }
            }
        }
    }
    //$_SESSION["products"] = "";

    die(json_encode($_SESSION)); //output json 
}

################## Get products in cart ###################
if (isset($_POST["getCart"])) {
    die(json_encode($_SESSION)); //output json
}

################## Order products in cart ###################
if (isset($_POST["placeOrder"])) {
	
	foreach ($_POST as $key => $value) {
            $post[$key] = filter_var($value, FILTER_SANITIZE_STRING); //create a new product array 
        }
	
	/* echo json_encode($_POST); //output json
	exit; */
	$data = Array ("firstName" => $post["first_name"],
						"lastName" => $post["last_name"],
						"email" => $post["email"],
						"phone" => $post["phone"],
						"address" => $post["address"],
						"total" => $_SESSION["products"]["total"]
		 );
        $id = $db->insert('orders', $data);
//        if ($id)
//			echo 'user was created. Id=' . $id;
//		else
//			echo 'insert failed: ' . $db->getLastError();
//                
//                exit;
		 
	foreach ($_SESSION as $products) {
		
		
		
		foreach ($products['ready'] as $productCI) {
				
				$data1 = Array ("orderId" => $id,
						"type" => 'ready',
						"productName" => $productCI['product_name'],
						"menuName" => $productCI['product_menu'],
						"qty" => "1"
				);
				
				$id1 = $db->insert ('cart_items', $data1);
				/* if ($id1)
					echo 'user was created. Id=' . $id1;
				else
					echo 'insert failed: ' . $db->getLastError(); */
			foreach ($productCI['product_ingridient'] as $productCII) {
					//print_r($productIt);exit;
					$data2 = Array ("productIngridient" => $productCII['name'],
							"qty" => $productCII['qty'],
							"price" => $productCII['price'],
							"step" => "",
							"cartItemsId" => $id1
					);
					
					$id2 = $db->insert ('cart_item_ingridients', $data2);
					/* if ($id2)
						echo 'user was created. Id=' . $id2;
					else
						echo 'insert failed: ' . $db->getLastError(); */
				
			}
		} 
		foreach ($products['wok'] as $productW => $productV) {
			
			$data1 = Array ("orderId" => $id,
						"type" => 'wok',
						"productName" => $productW,
						"qty" => $productV['qty']
				);
				
				$id1 = $db->insert ('cart_items', $data1);
				/* if ($id1)
					echo 'user was created. Id=' . $id1;
				else
					echo 'insert failed: ' . $db->getLastError(); */
			
			foreach ($productV['items'] as $product) {
				
				$data2 = Array ("productIngridient" => $product['product_name'],
							"qty" => $product['product_ingridient']['default']['qty'],
							"price" => $product['product_ingridient']['default']['price'],
							"step" => $product['product_step'],
							"cartItemsId" => $id1
					);
					
					$id2 = $db->insert ('cart_item_ingridients', $data2);
					/* if ($id2)
						echo 'user was created. Id=' . $id2;
					else
						echo 'insert failed: ' . $db->getLastError(); */
			}
		}
	}
    
       
    date_default_timezone_set('Etc/UTC');

    require 'phpmailer/PHPMailerAutoload.php';

    //Create a new PHPMailer instance
    $mail = new PHPMailer;

    //Tell PHPMailer to use SMTP
    $mail->isSMTP();

    //Enable SMTP debugging
    // 0 = off (for production use)
    // 1 = client messages
    // 2 = client and server messages
    $mail->SMTPDebug = 0;

    //Ask for HTML-friendly debug output
    $mail->Debugoutput = 'html';

    //Set the hostname of the mail server
    $mail->Host = 'smtp.gmail.com';
    // use
    // $mail->Host = gethostbyname('smtp.gmail.com');
    // if your network does not support SMTP over IPv6

    //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
    $mail->Port = 587;

    //Set the encryption system to use - ssl (deprecated) or tls
    $mail->SMTPSecure = 'tls';

    //Whether to use SMTP authentication
    $mail->SMTPAuth = true;

    //Username to use for SMTP authentication - use full email address for gmail
    $mail->Username = "manish.rane@interactive.in";

    //Password to use for SMTP authentication
    $mail->Password = "sunshineman";

    //Set who the message is to be sent from
    $mail->setFrom('orders@sillichilli.com', 'Orders');

    //Set an alternative reply-to address
    //$mail->addReplyTo('replyto@example.com', 'First Last');

    //Set who the message is to be sent to
    $mail->addAddress('manish.rane@interactive.in', 'Manish');

    //Set the subject line
    $mail->Subject = "Sillichilli : Order - ".$id;

    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    //$mail->msgHTML(file_get_contents("order.php?orderid=$id"), dirname(__FILE__));
    $ch = curl_init();
    $timeout = 100;
    curl_setopt($ch, CURLOPT_URL, SITE_URL."/order.php?orderId=".$id);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $data = curl_exec($ch);
    curl_close($ch);
    
    $mail->msgHTML($data);

    //Replace the plain text body with one created manually
    //$mail->AltBody = 'This is a plain-text message body';

    //Attach an image file
    //$mail->addAttachment('images/phpmailer_mini.png');

    //send the message, check for errors
    if (!$mail->send()) {
        //echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
        //echo "Message sent!";
    }
	
    $_SESSION['orderId'] = ""; 
    $_SESSION["products"] = "";
    die(json_encode(array('status' => 'Your order had been placed.'))); //output json
}



############# Modify products to session #########################
if (isset($_POST["currentWok"])) {
    $_SESSION["products"]['currentWok'] = "";
    die(json_encode($_SESSION)); //output json 
}

############# Modify products to session #########################
if (isset($_POST["wQty"])) {
    $wQty = $_POST["wQty"];
    $product_work = $_POST["product_work"];
    if ($wQty == "add") {
        $_SESSION["products"]['wok'][$product_work]['qty']++;
    }
    $wQtyCount = $_SESSION["products"]['wok'][$product_work]['qty'];
    if ($wQty == "remove") {
        if ($wQtyCount > 1) {
            $_SESSION["products"]['wok'][$product_work]['qty']--;
        } else {
            unset($_SESSION["products"]['wok'][$product_work]);
        }
    }
//    unset($_SESSION["products"]['wok'][$delWok]);
//    if ($delWok  == $_SESSION["products"]['currentWok']) {
//        $_SESSION["products"]['currentWok'] = "";
//    }
    $_SESSION["products"]["total"] = "";
    foreach ($_SESSION as $products) {
        foreach ($products['ready'] as $product) {
            foreach ($product['product_ingridient'] as $item => $value) {
                $_SESSION["products"]["total"] += $value['price'] * $value['qty'];
            }
        }
        foreach ($products['wok'] as $productW => $productV) {
            foreach ($productV['items'] as $product) {
                foreach ($product['product_ingridient'] as $item => $value) {
                    $_SESSION["products"]["total"] += ($value['price'] * $value['qty']) * $productV['qty'];
                }
            }
        }
    }
    die(json_encode($_SESSION)); //output json 
}