<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_resto_itemtoingrdient".
 *
 * @property integer $id
 * @property integer $itemId
 * @property integer $ingredientId
 * @property integer $price
 *
 * @property TblRestoIngredient $ingredient
 * @property TblRestoItem $item
 */
class Itemtoingrdient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_resto_itemtoingrdient';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['itemId', 'ingredientId', 'price'], 'required'],
            [['itemId', 'ingredientId', 'price'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
           /*  'itemId' => 'Item ID',
            'ingredientId' => 'Ingredient ID', */
            'item.name' => 'Item',
            'ingredient.name' => 'Ingredient',
            'price' => 'Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredient()
    {
        return $this->hasOne(Ingredient::className(), ['id' => 'ingredientId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Items::className(), ['id' => 'itemId']);
    }
}
