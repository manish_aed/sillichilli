<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_resto_ingredient".
 *
 * @property integer $id
 * @property string $name
 * @property integer $inOrder
 *
 * @property TblRestoItemtoingrdient[] $tblRestoItemtoingrdients
 */
class Ingredient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_resto_ingredient';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'inOrder'], 'required'],
            [['inOrder'], 'integer'],
            [['name'], 'string', 'max' => 4000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'inOrder' => 'In Order',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblRestoItemtoingrdients()
    {
        return $this->hasMany(TblRestoItemtoingrdient::className(), ['ingredientId' => 'id']);
    }
}
