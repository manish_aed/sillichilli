<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_resto_item".
 *
 * @property integer $id
 * @property string $name
 * @property string $subTitle
 * @property integer $chillies
 * @property string $price
 * @property integer $menuId
 * @property string $step
 * @property string $type
 * @property integer $new_item
 * @property string $unit
 * @property double $quantity
 * @property string $description
 *
 * @property TblRestoMenu $menu
 * @property TblRestoItemtoingrdient[] $tblRestoItemtoingrdients
 */
class Items extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_resto_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'menuId'], 'required'],
            [['chillies', 'menuId', 'new_item'], 'integer'],
            [['price', 'quantity'], 'number'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 4000],
            [['subTitle', 'step'], 'string', 'max' => 255],
            [['type', 'unit'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'subTitle' => 'Sub Title',
            'chillies' => 'Chillies',
            'price' => 'Price',
            'menuId' => 'Menu ID',
            'step' => 'Step',
            'type' => 'Type',
            'new_item' => 'New Item',
            'unit' => 'Unit',
            'quantity' => 'Quantity',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(TblRestoMenu::className(), ['id' => 'menuId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblRestoItemtoingrdients()
    {
        return $this->hasMany(TblRestoItemtoingrdient::className(), ['itemId' => 'id']);
    }
}
