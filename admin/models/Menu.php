<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_resto_menu".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 *
 * @property TblRestoItem[] $tblRestoItems
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_resto_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'type'], 'required'],
            [['id'], 'integer'],
            [['name', 'type'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblRestoItems()
    {
        return $this->hasMany(TblRestoItem::className(), ['menuId' => 'id']);
    }
}
