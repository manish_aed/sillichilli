<?php
use app\Models\Ingredient;
use app\Models\Items;
use yii\helpers\ArrayHelper;

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Models\Itemtoingrdient */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="itemtoingrdient-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php 	$items=Items::find()->all();
	
			$listItems=ArrayHelper::map($items,'id','name');

			echo $form->field($model, 'itemId')->dropDownList(
								$listItems, 
								['prompt'=>'Select...']); ?>

   
	<?php 	$Ingredient=Ingredient::find()->all();
	
			$listMenus=ArrayHelper::map($Ingredient,'id','name');

			echo $form->field($model, 'ingredientId')->dropDownList(
								$listMenus, 
								['prompt'=>'Select...']); ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
