<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Models\Itemtoingrdient */

$this->title = 'Create Itemtoingrdient';
$this->params['breadcrumbs'][] = ['label' => 'Itemtoingrdients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="itemtoingrdient-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
