<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\Models\Orders */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-view">

    <h1>Order No : <?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'firstName',
            'lastName',
            'email:email',
            'phone',
            'address:ntext',
            'total',
            'created',
        ],
    ]) ?>
	
	<?php
		
		//print_r($model->products);
		
		
		foreach ($model->products as $key => &$entry) {
			$orderDetails[$entry['menuName'].'|'.$entry['productName']][] = $entry;
		}
		/* echo "<pre>";
		print_r($orderDetails);
		echo "</pre>"; */
		
		foreach ($orderDetails as $orderKey=>$orders) { 
				$orderPrice = 0;
		?>
			
			<div class="panel panel-primary">
				<div class="panel-heading">
					<strong>
						<?php
							$productTitle = explode("|",$orderKey);
							
							trim($productTitle[0]);
							echo !empty($productTitle[0]) ? $productTitle[0].' <span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span> '.$productTitle[1] : 'Wok <span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span> '.$productTitle[1]; 
						?>
					</strong>
				</div>
			  <div class="panel-body">
				<?php foreach ($orders as $order) { ?>
					<p>
						<strong>
						<?php if ($order['step'] != 0 ) { ?>
							
							Step : 
							<?php 
								echo $order['step']." - ";
								echo $order['step'] == 1 ? "Wok Selected" : "";
								echo $order['step'] == 2 ? "Base" : "";
								echo $order['step'] == 3 ? "Sauce" : "";
								echo $order['step'] == 4 ? "Extras" : "";
							?>
							 <span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span> 
						
						<?php } ?>
					
					<?php echo $order['productIngridient']; ?></strong> : Rs. <?php echo $order['price']; ?> <span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span> 
					Quantity : <?php echo $order['itemQty']; ?> <span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span> 
					Subtotal : <?php echo $order['price']*$order['itemQty']; ?></p>
					<?php 
						$orderPrice = ($order['price']*$order['itemQty']) + $orderPrice; 
					?>
					
					<hr>
				<?php } ?>
				<p><strong class="text-success">Total : Rs. <?php echo $orderPrice; ?></strong></p>
			  </div>
			</div>
			
		<?php }
		
	?>
</div>
