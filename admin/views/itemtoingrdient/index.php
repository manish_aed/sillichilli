<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Itemtoingrdients';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="itemtoingrdient-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Itemtoingrdient', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            /* 'itemId',
            'ingredientId', */
            'item.name',
			'ingredient.name',
            'price',
            

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
