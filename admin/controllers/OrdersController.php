<?php

namespace app\controllers;

use Yii;
use app\Models\Orders;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\db\Query;

/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Orders::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		$queryOrder = new Query;
		$queryOrder->select(['*'])  
			->from('orders')
			->where(['id' => $id])
			; 
				
		$commandOrder = $queryOrder->createCommand();
		$orders = $commandOrder->queryOne();
		
		$queryProducts = new Query;
		$queryProducts->select([
				/* 'orders.id',
				'orders.firstName', 
				'orders.lastName',
				'orders.phone',
				'orders.address',
				'orders.total',
				'orders.created', */
				'cart_items.type',
				'cart_items.productName',
				'cart_items.menuName',
				'cart_items.qty as productQty',
				'cart_item_ingridients.productIngridient',
				'cart_item_ingridients.qty as itemQty',
				'cart_item_ingridients.price',
				'cart_item_ingridients.step'
				]
				)  
			->from('cart_items')
			->where(['cart_items.orderId' => $id])
			/* ->join('LEFT JOIN', 'cart_items',
						'orders.id =cart_items.orderId') */		
			->join('LEFT JOIN', 'cart_item_ingridients', 
						'cart_items.id =cart_item_ingridients.cartItemsId')
			; 
			
			//echo $queryProducts->createCommand()->sql;exit;
			//SELECT `cart_items`.`type`, `cart_items`.`productName`, `cart_items`.`menuName`, `cart_items`.`qty` AS `productQty`, `cart_item_ingridients`.`productIngridient`, `cart_item_ingridients`.`qty` AS `itemQty`, `cart_item_ingridients`.`price`, `cart_item_ingridients`.`step` FROM `cart_items` LEFT JOIN `cart_item_ingridients` ON cart_items.id =cart_item_ingridients.cartItemsId WHERE `cart_items`.`orderId`=:qp0
				
		$commandProducts = $queryProducts->createCommand();
		$orders["products"] = $commandProducts->queryAll();	
		
		/* echo "<pre>";
		print_r($orders);
		echo "<pre>";
		exit; */
		
        return $this->render('view', [
                'model' => (object) $orders,
            ]);
    }

    /**
     * Creates a new Orders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Orders();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Orders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Orders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
