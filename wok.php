<?php
session_start(); //start session
include_once 'db.php';
//session_destroy();
$db->where("tbl_resto_item.step", 1);
$stepOne = $db->get('tbl_resto_item');

$db->where("tbl_resto_item.step", 2);
$stepTwo = $db->get('tbl_resto_item');

$db->where("tbl_resto_item.step", 3);
$stepThree = $db->get('tbl_resto_item');

$db->where("tbl_resto_item.step", 4);
$stepFour = $db->get('tbl_resto_item');

$thisPage = "wok";

//echo "<pre>";
////print_r($_SESSION);
//foreach ($_SESSION as $products) {
//    foreach($products['ready'] as $product){
//        foreach($product['product_ingridient'] as $item=>$value){
//            echo $value['price']*$value['qty']."|";
//        }
//    }
//    foreach($products['wok'] as $productW=>$productV){
//        //print_r($productW);
//        foreach($productV as $wokK=>$wokV){
//            //print_r($product);
//            //echo $wokK;
//            foreach($wokV['product_ingridient'] as $item=>$value){
//                echo $value['price']*$value['qty']."|";
//            }
//        }
//    }
//}
//exit;
//$queryMenus = "SELECT * FROM tbl_resto_menu";
//$resultMenus = $db->query($queryMenus);
//while ($rowmenus = $resultMenus->fetch_assoc()) {
//    $menus[] = $rowmenus;
//}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title></title>

        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="css/materialize.min.css">

        <!-- bxSlider CSS file
        <link href="css/jquery.bxslider.css" rel="stylesheet" /> -->
        <link href="css/jquery-ui.min.css" rel="stylesheet" />

        <link href="css/style.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->



    </head>
    <body>
        <div class="menuBg">
        </div>
        <div class="wokCon">
            <?php include "header.php"; ?>

            <div class="mainCon" style="position: relative">

                

                <div class="steps">

                    <ul class="tabs">
                        <li class="tab tab1"><a href="#step1">Step 1</a></li>
                        <li class="tab tab2"><a href="#step2">Step 2</a></li>
                        <li class="tab tab3"><a href="#step3">Step 3</a></li>
                        <li class="tab tab4"><a href="#step4">Step 4</a></li>
                        <!--	<li class="tab col s3 disabled"><a href="#test3">Disabled Tab</a></li>
                        <li class="tab col s3"><a href="#test4">Test 4</a></li>	-->
                    </ul>

                    <div id="step1" class="stepBox">
                        <h2 class="stepLabel">
                            <img class="stepNo" src="images/step1.png" /> Choose Your Wok
                        </h2>

                        <div class="itemBoxContainer">

                            <?php
                            foreach ($stepOne as $sOne) {
                                $i = 1;
                                ?>

                                <div id="itemWok<?php echo $i; ?>" class="itemBox">
                                    <form class='form-item'>
                                        <img class="itemPic" src="images/<?php echo str_replace(" ", "", $sOne['name']); ?>.png" />
                                        <h5 class="itemLabel"><span class='pname'><?php echo $sOne['name']; ?></span> <span>Rs. <?php echo $sOne['price']; ?></span> 
                                            <a href="javascript:void(0)"><input class="addToKadhai" type='image' src='images/addCircle.png'></a>
                                        </h5>

                                        <?php
                                        echo "<input type='hidden' value='$sOne[id]' name='product_code'>";
                                        echo "<input type='hidden' value='$sOne[price]' class='product_price' name='product_price'>";
                                        echo "<input type='hidden' value='default' name='product_ingridient'>";
                                        echo "<input type='hidden' value='wok' name='product_type'>";
                                        echo "<input type='hidden' value='$sOne[step]' name='product_step'>";
                                        $i++;
                                        ?>
                                    </form>
                                </div>
                                <?php
                            }
                            ?>

                            

                        </div>
                    </div>

                    <div id="step2" class="stepBox">
                        <h2 class="stepLabel">
                            <img class="stepNo" src="images/step2.png" /> Select Your Base (any one)
                        </h2>

                        <div class="itemBoxContainer">

                            <?php
                            foreach ($stepTwo as $sTwo) {
                                $i = 1;
                                ?>

                                <div id="itemWok<?php echo $i; ?>" class="itemBox">
                                    <form class='form-item'>
                                        <img class="itemPic" src="images/<?php echo str_replace(" ", "", $sTwo['name']); ?>.png" />
                                        <h5 class="itemLabel"><span class='pname'><?php echo $sTwo['name']; ?></span> <span>Rs. <?php echo $sTwo['price']; ?></span> 
                                            <a href="javascript:void(0)"><input class="addToKadhai" type='image' src='images/addCircle.png'></a>
                                        </h5>

                                        <?php
                                        echo "<input type='hidden' value='$sTwo[id]' name='product_code'>";
                                        echo "<input type='hidden' value='$sTwo[price]' class='product_price' name='product_price'>";
                                        echo "<input type='hidden' value='default' name='product_ingridient'>";
                                        echo "<input type='hidden' value='wok' name='product_type'>";
                                        echo "<input type='hidden' value='$sTwo[step]' name='product_step'>";
                                        $i++;
                                        ?>
                                    </form>
                                </div>
                                <?php
                            }
                            ?>

                            

                        </div>


                    </div>

                    <div id="step3" class="stepBox">
                        <h2 class="stepLabel">
                            <img class="stepNo" src="images/step2.png" /> Select Your Sauce (any one)
                        </h2>

                        <div class="itemBoxContainer">
                            <?php
                            foreach ($stepThree as $sThree) {
                                $i = 1;
                                ?>

                                <div id="itemWok<?php echo $i; ?>" class="itemBox">
                                    <form class='form-item'>
                                        <img class="itemPic" src="images/<?php echo str_replace(" ", "", $sThree['name']); ?>.png" />
                                        <h5 class="itemLabel"><span class='pname'><?php echo $sThree['name']; ?></span> <span>Rs. <?php echo $sThree['price']; ?></span> 
                                            <a href="javascript:void(0)"><input class="addToKadhai" type='image' src='images/addCircle.png'></a>
                                        </h5>

                                        <?php
                                        echo "<input type='hidden' value='$sThree[id]' name='product_code'>";
                                        echo "<input type='hidden' value='$sThree[price]' class='product_price' name='product_price'>";
                                        echo "<input type='hidden' value='default' name='product_ingridient'>";
                                        echo "<input type='hidden' value='wok' name='product_type'>";
                                        echo "<input type='hidden' value='$sThree[step]' name='product_step'>";
                                        $i++;
                                        ?>
                                    </form>
                                </div>
                                <?php
                            }
                            ?>
                            



                        </div>


                    </div>



                    <div id="step4" class="stepBox">
                        <h2 class="stepLabel">
                            Extras
                        </h2>

                        <div class="extraItemBlockContainer">

                            <ul class="extraItemBlock">

                                <?php
                                foreach ($stepFour as $sFour) {
                                    $i = 1;
                                    ?>
                                    <li class="extraItem itemBox">
                                        <form class='form-item'>
                                            <span class="extraItemPic">
                                                <img class="itemPic" src="images/<?php echo str_replace(" ", "", $sFour['name']); ?>.png" />
                                            </span>
                                            <span class="extraItemName pname">
                                                <?php echo $sFour['name']; ?>
                                            </span>
                                            <span class="extraItemPrice">
                                                Rs. <?php echo $sFour['price']; ?>
                                            </span>
                                            <span class="extraItemAdd">
                                                <a href="javascript:void(0)"><input class="addToKadhai" type='image' src='images/addCircle.png'></a>
                                            </span>

                                            <?php
                                            echo "<input type='hidden' value='$sFour[id]' name='product_code'>";
                                            echo "<input type='hidden' value='$sFour[price]' class='product_price' name='product_price'>";
                                            echo "<input type='hidden' value='default' name='product_ingridient'>";
                                            echo "<input type='hidden' value='wok' name='product_type'>";
                                            echo "<input type='hidden' value='$sFour[step]' name='product_step'>";
                                            $i++;
                                            ?>

                                        </form>
                                    </li>
                                    <?php
                                }
                                ?>

                                

                            </ul>
                            <button id="completeWok" class="waves-effect waves-green btn-flat red eHide" style="color:white">Complete My Wok</button>
                            <div class="clearB"></div>
                        </div>

                    </div>
                    <img src="images/wok-kadhai.png" class="wok-kadhai" />
                </div>
                
                <?php include "cart.php"; ?>

                

            </div>
            <?php include "footer.php"; ?>

        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery.min.js"></script>
        <script src="js/TweenMax.min.js"></script>
                <!-- <script src="js/jquery.bxslider.min.js"></script> -->
        <script src="js/jquery.easytabs.min.js" type="text/javascript"></script>
        <script src="js/jquery-ui.min.js" type="text/javascript"></script>

        <script src="js/materialize.min.js"></script>
        <script src="js/common.js"></script>
        <script>
            $(document).ready(function () {
                
                window.cartCount = 0;

                $.ajax({//make ajax request to cart_process.php
                    url: "cart_process.php",
                    type: "POST",
                    dataType: "json", //expect json value from server
                    data: "getCart=1"
                }).done(function (data) { //on Ajax success
                    cartDisplay(data);
                    //alert(Object.keys(data.products).length);
                    cartPop(data);
                })
                
                $("#placeOrderForm").submit(function (e) {
                    e.preventDefault();
                    var form_data = "placeOrder=1";
                    $.ajax({//make ajax request to cart_process.php
                            url: "cart_process.php",
                            type: "POST",
                            dataType: "json", //expect json value from server
                            data: form_data
                        }).done(function (data) { //on Ajax success
                            cartDisplay(data);
                            $(".OrderformCon").html(data.status);
                            setTimeout(function(){ location.reload();  }, 2000);
                        });
                })
                
                $(".form-item").submit(function (e) {
                    e.preventDefault();
                    var form_data = $(this).serialize() + "&add=1";
                    var button_content = $(this).find('button[type=submit]');
                    button_content.html('Adding...'); //Loading button text 

                    var productPrice = $(this).find(".product_price").val();
                    var productName = $(this).find(".pname").html();

                    if (productPrice !== "") {
                        $.ajax({//make ajax request to cart_process.php
                            url: "cart_process.php",
                            type: "POST",
                            dataType: "json", //expect json value from server
                            data: form_data
                        }).done(function (data) { //on Ajax success
                            cartDisplay(data);
                            cartPop(data);
                            Materialize.toast(productName+' Added!', 3000)
                        });
                        e.preventDefault();
                    } else {
                        //alert("Please select item.");
                        $(this).append("<p class='error'>Please select item.</p>");
                        e.preventDefault();
                    }
                });

                $(".itemBox .form-item").click(function (e) {
                    $(this).submit();
                });
                $("#completeWok").click(function (e) {
                    var form_data = "currentWok=1";
                    $.ajax({//make ajax request to cart_process.php
                            url: "cart_process.php",
                            type: "POST",
                            dataType: "json", //expect json value from server
                            data: form_data
                        }).done(function (data) { //on Ajax success
                            cartDisplay(data);
                            cartPop(data);
                        });
                        e.preventDefault();
                });


                $("#cart-info").on('click', 'span.qtyWork', function (e) {
                    e.preventDefault();
                    var pWok = $(this).attr("data-product-work"); //get product code
                    var pQty = $(this).attr("data-product-qty"); //get product code
                    var form_data = "wQty="+pQty+"&product_work="+pWok;
                    $.ajax({//make ajax request to cart_process.php
                            url: "cart_process.php",
                            type: "POST",
                            dataType: "json", //expect json value from server
                            data: form_data
                        }).done(function (data) { //on Ajax success
                            cartDisplay(data);
                            cartPop(data);
                        });
                });   
                
                $("#cart-info").on('click', 'span.qtyUpdate', function (e) {
                    e.preventDefault();
                    var pQty = $(this).attr("data-product-qty"); //get product code
                    var pCode = $(this).attr("data-product-code"); //get product code
                    var pIngridient = $(this).attr("data-product-ingridient"); //get product code
                    var pType = $(this).attr("data-product-type"); //get product code
                    var pStep = $(this).attr("data-product-step"); //get product code

                    //alert(pQty); return false;
                    if (pType == "ready") {
                        var formData = "qty=" + pQty + "&product_code=" + pCode + "&product_ingridient=" + pIngridient + "&product_type=" + pType;

                        $.ajax({//make ajax request to cart_process.php
                            url: "cart_process.php",
                            type: "POST",
                            dataType: "json", //expect json value from server
                            data: formData
                        }).done(function (data) { //on Ajax success
                            cartDisplay(data);
                            //alert(data);
                            cartPop(data);
                        });
                    }

                    if (pType == "wok") {
                        var formData = "product_step=" + pStep + "&qty=" + pQty + "&product_code=" + pCode + "&product_ingridient=" + pIngridient + "&product_type=" + pType;
                        //alert(formData);return false;
                        $.ajax({//make ajax request to cart_process.php
                            url: "cart_process.php",
                            type: "POST",
                            dataType: "json", //expect json value from server
                            data: formData
                        }).done(function (data) { //on Ajax success
                            cartDisplay(data);
                            //alert(data);
                            cartPop(data);
                        });
                    }

                });
                
                $("#checkout").click(function (e) {
                    if (window.cartCount == 1) {
                        $('#orderPop').openModal();
                        //$('#checkout').leanModal();
                    } else {
                        $('#cartEmpty').openModal();
                    }
                })
                
            });
            
            function cartPop(data) {
                if (data.products.total === 0 || data.products.total === "" || data.products === "" || data.products.currentWok != "") {
                    //Object.keys(data.products).length;
                    window.cartCount = 0;
                    
                    if (data.products.currentWok != "") {
                        $(".cartEMessage").html("Please complete your wok.");
                    } else {
                        $(".cartEMessage").html("Your cart is empty.");
                    }
                    
                } else {
                    window.cartCount = 1;
                }
            }
            
            
            function cartDisplay(data) {
                $("#cart-info").html("");
                if (data.products !== undefined) {
                    if (data.products.ready !== undefined) {
                        jQuery.each(data.products.ready, function (index, value) {
                            //console.log('element at index ' + index + ' is ' + value);
                            //alert(value.product_name);
                            $("#cart-info").prepend("<div id='item"+index+"' class='itemCartCon'></div>");
                            
                            $("#cart-info #item"+index).append('<p><strong>' + value.product_name + '</strong></p>');
                            jQuery.each(value.product_ingridient, function (index1, value1) {
                                //htmlData =+ '<p><strong>'+index1+'</strong></p>';
                                if (index1 === "default") {
                                    var subMenuText = "";
                                } else {
                                    var subMenuText = index1 + " :";
                                }
                                $("#cart-info #item"+index).append('<p><span class="btnSM qtyUpdate" data-product-type="ready" data-product-code="' + index + '" data-product-ingridient="' + index1 + '" data-product-qty="remove">-</span> <span class="btnSM qtyUpdate" data-product-type="ready" data-product-code="' + index + '" data-product-ingridient="' + index1 + '" data-product-qty="add">+</span>' + subMenuText + ' Rs. ' + value1.price + ' x ' + value1.qty + '</p>');
                                //console.log('element at index1 ' + index1 + ' is ' + value1);
                                jQuery.each(value1, function (index2, value2) {
                                    //console.log('element at index2 ' + index2 + ' is ' + value2);
                                });
                            });
                        });
                    }

                    if (data.products.wok !== undefined) {


                        jQuery.each(data.products.wok, function (indexW, valueW) {
                            

                            // Current WOK
                            if (data.products.currentWok === indexW) {
                                $("#cart-info").prepend("<div id='"+indexW+"' class='wokCartCon'></div>");
                                $("#cart-info #"+indexW).append("<p class='t"+indexW+" wTitle'></p>");
                                $("#cart-info #"+indexW+" .t"+indexW).html('<strong>' + indexW + ' x '+valueW.qty+'</strong>');
                                
                                jQuery.each(valueW.items, function (index, value) {
                                    //console.log('element at index ' + index + ' is ' + value);
                                    //alert(value.product_name);
                                    var step;
                                    if (value.product_step == 4) {
                                        $("#completeWok").removeClass("eHide");
                                        step = "Extras";
                                    } else {
                                        step = "Step " + value.product_step;
                                        $("#completeWok").addClass("eHide");
                                    }
                                    $("#cart-info #"+indexW+" ").append("<p class='cstep" + value.product_step + "'></p>");
                                    $("#cart-info #"+indexW+" .cstep" + value.product_step).html('<strong>' + step + '</strong>');
                                    $("#cart-info #"+indexW+" ").append('<p class="cname"><strong>' + value.product_name + '</strong></p>');
                                    jQuery.each(value.product_ingridient, function (index1, value1) {
                                        //htmlData =+ '<p><strong>'+index1+'</strong></p>';
                                        if (index1 === "default") {
                                            var subMenuText = "";
                                        } else {
                                            var subMenuText = index1 + " :";
                                        }
                                        var qtyOpt;
                                        if (value.product_step == 4) {
                                            qtyOpt = '<span class="btnSM qtyUpdate" data-product-step="' + value.product_step + '" data-product-type="wok" data-product-code="' + index + '" data-product-ingridient="' + index1 + '" data-product-qty="remove">-</span> <span class="btnSM qtyUpdate" data-product-step="' + value.product_step + '" data-product-type="wok" data-product-code="' + index + '" data-product-ingridient="' + index1 + '" data-product-qty="add">+</span>';
                                        } else {
                                            qtyOpt = '';
                                        }
                                        $("#cart-info #"+indexW+" ").append('<p>' + qtyOpt + subMenuText + ' Rs. ' + value1.price + ' x ' + value1.qty + '</p>');
                                        //console.log('element at index1 ' + index1 + ' is ' + value1);
                                        jQuery.each(value1, function (index2, value2) {
                                            //console.log('element at index2 ' + index2 + ' is ' + value2);
                                        });
                                    });
                                });
                            } else {
                                $("#cart-info").prepend("<div id='"+indexW+"' class='wokCartCon'></div>");
                                $("#cart-info #"+indexW).append("<p class='t"+indexW+" wTitle'></p>");
                                $("#cart-info #"+indexW+"  .t"+indexW).html('<strong>' + indexW + ' x '+valueW.qty+'</strong><span class="btnSM qtyWork" data-product-work="' + indexW + '" data-product-qty="add">+</span><span class="btnSM qtyWork" data-product-work="' + indexW + '" data-product-qty="remove">-</span>');
                                
                                jQuery.each(valueW.items, function (index, value) {
                                    //console.log('element at index ' + index + ' is ' + value);
                                    //alert(value.product_name);
                                    var step;
                                    if (value.product_step == 4) {
                                        step = "Extras";
                                        $("#completeWok").addClass("eHide");
                                    } else {
                                        step = "Step " + value.product_step;
                                        $("#completeWok").addClass("eHide");
                                    }
                                    $("#cart-info #"+indexW+" ").append("<p class='cstep" + value.product_step + "'></p>");
                                    $("#cart-info #"+indexW+" .cstep" + value.product_step).html('<strong>' + step + '</strong>');
                                    $("#cart-info #"+indexW+" ").append('<p class="cname"><strong>' + value.product_name + '</strong></p>');
                                    jQuery.each(value.product_ingridient, function (index1, value1) {
                                        //htmlData =+ '<p><strong>'+index1+'</strong></p>';
                                        if (index1 === "default") {
                                            var subMenuText = "";
                                        } else {
                                            var subMenuText = index1 + " :";
                                        }
                                        
                                        $("#cart-info #"+indexW+" ").append('<p>' + subMenuText + ' Rs. ' + value1.price + ' x ' + value1.qty + '</p>');
                                        //console.log('element at index1 ' + index1 + ' is ' + value1);
                                        jQuery.each(value1, function (index2, value2) {
                                            //console.log('element at index2 ' + index2 + ' is ' + value2);
                                        });
                                    });
                                });
                            }

                        });



                    }

                    $(".total").html('Total : Rs. <span>' + data.products.total + '</span>');

                    if (data.products.total === 0 || data.products.total === "" || data.products === "") {
                        $("#cart-info").html('<p>Your cart is empty.</p>');
                         $(".total").html("");
                    }
                } else {
                    $("#cart-info").html('<p>Your cart is empty.</p>');
                    $(".total").html("");
                }
            }

            function selectPrice(element, price, name, current) {
                $(".inPrice").removeClass("selectPrice");
                $(".product_price").val("");

                $(element + " .inPrice" + name).addClass("selectPrice");
                $("input[type=radio]").prop('checked', false);
                $(current).prop('checked', true);

                $(element + " .product_price").val(price);

                $(".error").remove();
            }
        </script>

        <script>
            $(window).load(function () { // wait for document ready

                /* var slider = $( ".steps" ).bxSlider({
                 controls: false
                 }); */
                //$('ul.tabs').tabs();
                $('.steps').easytabs({
                    updateHash: false,
                    transitionIn: "slideDown"
                });

                //$(".cartBox").draggable({containment: "body", scroll: false, handle: ".orderTitle"});

                var kadhai = $(".wok-kadhai");
                var kadhaiW = kadhai.width();
                var kadhaiH = kadhai.height();
                //alert(kadhaiW);

                $(".itemBox").click(function () {

                    var kadhaiOffset = kadhai.offset();
                    var kadhaiLeft = kadhaiOffset.left + (kadhaiW / 2);
                    var kadhaiTop = kadhaiOffset.top + (kadhaiH / 2);

                    var itemBox = $(this);
                    var item = itemBox.find(".itemPic");
                    var itemClone = item.clone(true);
                    var styles = {
                        position: "absolute",
                        top: "0",
                        left: "50%",
                        marginLeft: "-40px"
                    };
                    itemClone.css(styles);
                    itemClone.appendTo(itemBox);

                    var itemW = item.width();
                    var itemH = item.height();
                    var itemOffset = item.offset();
                    var itemLeft = itemOffset.left + (itemW / 2);
                    var itemTop = itemOffset.top + (itemH / 2);

                    //alert(kadhaiH);
                    //item.css("z-index", "10000000");

                    var xDist = kadhaiLeft - itemLeft;
                    var yDist = kadhaiTop - itemTop;
                    TweenMax.to(itemClone, 1, {x: xDist, y: yDist, scale: 0.8, zIndex: 10000, opacity: 0, onComplete: next, onCompleteParams: [item]});
                });

                function next(clickedItem) {
                    /* var totalSlides = slider.getSlideCount();
                     var currentSlide = slider.getCurrentSlide();
                     //alert(totalSlides);
                     if(currentSlide != (totalSlides-1)){
                     slider.goToNextSlide();
                     } */
                    //$('ul.tabs').tabs('select_tab', 'step2');
                    if (clickedItem.parents('#step1').length != 0) {
                        //$('.steps').easytabs('select', '#step3');
                        $('.tab2 a').click();
                    }
                    if (clickedItem.parents('#step2').length != 0) {
                        //alert("ok");
                        //$('.steps').easytabs('select', '#step3');
                        $('.tab3 a').click();
                    }
                    else if (clickedItem.parents('#step3').length != 0) {
                        $('.tab4 a').click();
                        //$('.tab1 a').click();
                    }
                }

            });
        </script>
    </body>
</html>