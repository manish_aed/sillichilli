<?php
session_start(); //start session
include_once("config.inc.php"); //include config file
include 'db.php';
setlocale(LC_MONETARY, "en_US"); // US national format (see : http://php.net/money_format)

$orderId = $_GET['orderid'];

$db->where ('id', $orderId);
$orders = $db->getOne('orders');

$db->join("cart_item_ingridients n", "i.id =n.cartItemsId", "LEFT");
$db->where("i.orderId", $orderId);
$products = $db->get("cart_items i", null, 	"i.type,
											i.productName,
											i.menuName,
											i.qty as productQty,
											n.productIngridient,
											n.qty as itemQty,
											n.price,
											n.step");

?>
<div style="font-family: Arial; width:600px; margin:auto">
<div style="font-size:14px;background:#B1D629;padding:15px">
<div><img src="http://interactive.in/clients/sillichilli/images/logo.png" width="150"></div>
<p>Dear <?php echo $orders['firstName']." ".$orders['lastName']; ?>,<br>
	Thank you for your order.
</p>
<p><strong>Your Order Details :</strong> Order No. <?php echo $orderId; ?><br>
<hr style="border:1px solid #fff">
	<?php echo $orders['firstName']." ".$orders['lastName']; ?>,<br>
	<?php echo $orders['address']; ?>,<br>
	Tel : <?php echo $orders['phone']; ?>, E-mail : <?php echo $orders['email']; ?>
</p>
<?php
		
		//print_r($products);
		
		
		foreach ($products as $key => &$entry) {
			$orderDetails[$entry['menuName'].'|'.$entry['productName']][] = $entry;
		}
		/* echo "<pre>";
		print_r($orderDetails);
		echo "</pre>"; */
		
		foreach ($orderDetails as $orderKey=>$ordersData) { 
				$orderPrice = 0;
		?>
			
			<div class="panel panel-primary" style="border:1px solid #D4ED6F; padding:5px; margin:5px 0">
				<div class="panel-heading">
					<strong style="color:#EB1C23">
						<?php
							$productTitle = explode("|",$orderKey);
							
							trim($productTitle[0]);
							echo !empty($productTitle[0]) ? $productTitle[0].' > '.$productTitle[1] : 'Wok > '.$productTitle[1]; 
						?>
					</strong>
				</div>
				<hr style="border:1px solid #5BB846">
			  <div class="panel-body">
				<?php foreach ($ordersData as $order) { ?>
					<p>
						<strong>
						<?php if ($order['step'] != 0 ) { ?>
							
							Step : 
							<?php 
								echo $order['step']." - ";
								echo $order['step'] == 1 ? "Wok Selected" : "";
								echo $order['step'] == 2 ? "Base" : "";
								echo $order['step'] == 3 ? "Sauce" : "";
								echo $order['step'] == 4 ? "Extras" : "";
							?>
							 >
						
						<?php } ?>
					
					<?php echo $order['productIngridient']; ?></strong> : Rs. <?php echo $order['price']; ?> > 
					Quantity : <?php echo $order['itemQty']; ?> > 
					Subtotal : <?php echo $order['price']*$order['itemQty']; ?></p>
					<?php 
						$orderPrice = ($order['price']*$order['itemQty']) + $orderPrice; 
					?>
					
					
				<?php } ?>
				<p><strong style="color:#EB1C23">Total : Rs. <?php echo $orderPrice; ?></strong></p>
			  </div>
			</div>
			
		<?php }
		
	?>
	
</div>
<div style="padding:20px; text-align: right; color:#fff; background:#EB1C23;">Order Total : Rs. <?php echo $orders['total']; ?></div>
</div>
<?php
exit;
echo "<pre>";
print_r($_SESSION);
exit;
foreach ($_SESSION as $products) {
    $data = Array ("firstName" => "John",
                    "lastName" => 'Doe',
                    "email" => 'man@man.com',
                    "phone" => '9874563210',
                    "address" => 'aaa',
                    "total" => $products['total']
     );
	 $id = $db->insert ('orders', $data);
    if ($id)
        echo 'user was created. Id=' . $id;
    else
        echo 'insert failed: ' . $db->getLastError();
    
    foreach ($products['ready'] as $productCI) {
			
			$data1 = Array ("orderId" => $id,
                    "type" => 'ready',
                    "productName" => $productCI['product_name'],
                    "qty" => "1"
			);
			
            $id1 = $db->insert ('cart_items', $data1);
			if ($id1)
				echo 'user was created. Id=' . $id1;
			else
				echo 'insert failed: ' . $db->getLastError();
        foreach ($productCI['product_ingridient'] as $productCII) {
				//print_r($productIt);exit;
				$data2 = Array ("productIngridient" => $productCII['name'],
						"qty" => $productCII['qty'],
						"price" => $productCII['price'],
						"step" => "",
						"cartItemsId" => $id1
				);
				
				$id2 = $db->insert ('cart_item_ingridients', $data2);
				if ($id2)
					echo 'user was created. Id=' . $id2;
				else
					echo 'insert failed: ' . $db->getLastError();
			
        }
    } 
    foreach ($products['wok'] as $productW => $productV) {
		
		$data1 = Array ("orderId" => $id,
                    "type" => 'wok',
                    "productName" => $productW,
                    "qty" => $productV['qty']
			);
			
            $id1 = $db->insert ('cart_items', $data1);
			if ($id1)
				echo 'user was created. Id=' . $id1;
			else
				echo 'insert failed: ' . $db->getLastError();
		
        foreach ($productV['items'] as $product) {
			
			$data2 = Array ("productIngridient" => $product['product_name'],
						"qty" => $product['product_ingridient']['default']['qty'],
						"price" => $product['product_ingridient']['default']['price'],
						"step" => $product['product_step'],
						"cartItemsId" => $id1
				);
				
				$id2 = $db->insert ('cart_item_ingridients', $data2);
				if ($id2)
					echo 'user was created. Id=' . $id2;
				else
					echo 'insert failed: ' . $db->getLastError();
        }
    }
}

?>
