/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function () { // wait for document ready

    // Footer Call Open Close
    $(".callImg").click(function () {
        if ($(".footInfo").hasClass("opened")) {
            $(".footInfo").animate({
                left: "-435px"
            }, {
                duration: 500,
                specialEasing: {
                    left: "linear"
                },
                complete: function () {
                    //$( this ).after( "<div>Animation complete.</div>" );
                    $(".footInfo").removeClass("opened");
                }
            });
        } else {
            $(".footInfo").animate({
                left: "-30px"
            }, {
                duration: 500,
                specialEasing: {
                    left: "linear"
                },
                complete: function () {
                    //$( this ).after( "<div>Animation complete.</div>" );
                    $(".footInfo").addClass("opened");
                }
            });
        }
    });

    $(".menuItem")
            .mouseenter(function () {
                $(this).animate({
                    right: "0"
                }, {
                    duration: 300,
                    specialEasing: {
                        left: "linear"
                    },
                    complete: function () {

                    }
                });
            })
            .mouseleave(function () {
                $(this).animate({
                    right: "-100px"
                }, {
                    duration: 300,
                    specialEasing: {
                        left: "linear"
                    },
                    complete: function () {

                    }
                });
            });

});
