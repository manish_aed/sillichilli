<?php 
session_start(); //start session
include_once 'db.php';

$menus = $db->get('tbl_resto_menu');

//$queryMenus = "SELECT * FROM tbl_resto_menu";
//$resultMenus = $db->query($queryMenus);
//while ($rowmenus = $resultMenus->fetch_assoc()) {
//    $menus[] = $rowmenus;
//}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title></title>
        
        <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" charset="utf-8" />
        <link href="css/style.css" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="menuBg">
        </div>    
            <?php include "header.php"; ?>

            <div class="galleryCon">
                <h1>Photo Gallery</h1>
                <div class="polaroid-images">
                        <a href="images/gallery/1.jpeg" rel='prettyPhoto[gal]'><img height="200" src="images/gallery/1.jpeg" /></a>
                        <a href="images/gallery/1.jpeg" rel='prettyPhoto[gal]'><img height="200" src="images/gallery/1.jpeg" /></a>
                        <a href="images/gallery/1.jpeg" rel='prettyPhoto[gal]'><img height="200" src="images/gallery/1.jpeg" /></a>
                        <a href="images/gallery/1.jpeg" rel='prettyPhoto[gal]'><img height="200" src="images/gallery/1.jpeg" /></a>
                        <a href="images/gallery/1.jpeg" rel='prettyPhoto[gal]'><img height="200" src="images/gallery/1.jpeg" /></a>
                        <a href="images/gallery/1.jpeg" rel='prettyPhoto[gal]'><img height="200" src="images/gallery/1.jpeg" /></a>
                        <a href="images/gallery/1.jpeg" rel='prettyPhoto[gal]'><img height="200" src="images/gallery/1.jpeg" /></a>
                        <a href="images/gallery/1.jpeg" rel='prettyPhoto[gal]'><img height="200" src="images/gallery/1.jpeg" /></a>
                        <a href="images/gallery/1.jpeg" rel='prettyPhoto[gal]'><img height="200" src="images/gallery/1.jpeg" /></a>
                        <a href="images/gallery/1.jpeg" rel='prettyPhoto[gal]'><img height="200" src="images/gallery/1.jpeg" /></a>
                        <a href="images/gallery/1.jpeg" rel='prettyPhoto[gal]'><img height="200" src="images/gallery/1.jpeg" /></a>
                        <a href="images/gallery/1.jpeg" rel='prettyPhoto[gal]'><img height="200" src="images/gallery/1.jpeg" /></a>
                        <a href="images/gallery/1.jpeg" rel='prettyPhoto[gal]'><img height="200" src="images/gallery/1.jpeg" /></a>
                        <a href="images/gallery/1.jpeg" rel='prettyPhoto[gal]'><img height="200" src="images/gallery/1.jpeg" /></a>
                        <a href="images/gallery/1.jpeg" rel='prettyPhoto[gal]'><img height="200" src="images/gallery/1.jpeg" /></a>
                        <a href="images/gallery/1.jpeg" rel='prettyPhoto[gal]'><img height="200" src="images/gallery/1.jpeg" /></a>
                </div>
                
            </div>
            <?php include "footer.php"; ?>
        

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery.min.js"></script>
        <script src="js/TweenMax.min.js"></script>
        <!-- <script src="js/ScrollMagic.min.js"></script>
        <script src="js/animation.gsap.min.js"></script>
        <script src="js/debug.addIndicators.min.js"></script>
        <script src="js/jquery.jparallax.min.js"></script> -->
        <script type="text/javascript" src="js/plax.js"></script>
        <script src="js/common.js"></script>
        <script src="js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
        <script>
            $(document).ready(function(){
                $("a[rel^='prettyPhoto']").prettyPhoto({social_tools:false});
            });
        </script>
    </body>
</html>