<?php
session_start(); //start session
include 'db.php';

//session_destroy();

//echo "<pre>";
////print_r($_SESSION);
//foreach ($_SESSION as $products) {
//    foreach($products['ready'] as $product){
//        foreach($product['product_ingridient'] as $item=>$value){
//            echo $value['price']*$value['qty']."|";
//        }
//    }
//}
//exit;

$menuId = $_REQUEST["menuId"];

if ($menuId == 6) {
    header("Location: wok.php?menuId=$menuId");
    exit;
}

$db->where("tbl_resto_menu.id", $menuId);
$menu = $db->getOne('tbl_resto_menu');

$db->where("tbl_resto_item.menuId", $menuId);
$items = $db->get('tbl_resto_item');

for ($a = 0; count($items) > $a; $a++) {

    $db->join("tbl_resto_ingredient", "tbl_resto_ingredient.Id=tbl_resto_itemtoingrdient.ingredientId", "INNER");
    $db->join("tbl_resto_item", "tbl_resto_item.Id=tbl_resto_itemtoingrdient.itemId", "INNER");
    $db->where("tbl_resto_item.id=tbl_resto_itemtoingrdient.itemId");
    $db->where("tbl_resto_item.menuId", $menuId);
    $db->where("tbl_resto_item.id", $items[$a]['id']);
    $db->orderBy("tbl_resto_ingredient.inOrder", "asc");
    $itemDet = $db->get("tbl_resto_itemtoingrdient", null, "tbl_resto_itemtoingrdient.price, tbl_resto_itemtoingrdient.itemId, tbl_resto_ingredient.name");
    $items[$a]['inData'] = $itemDet;
}
//echo "<pre>";print_r ($items);
//exit;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title></title>

        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="css/materialize.min.css">

        <link href="css/style.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="menuBg">
        </div>
        <?php include "header.php"; ?>

        <div class="mainCon">



            <img class="itemImgLeft" src="images/allTimeFav.png">
            <div class="itemList">
                
                <h1>
                    <?php echo $menu['name']; ?>
                </h1>


                <ul>
                    <!--                            <li>
                                                    <div class='itemPrice'> 
                                                        160/190 <a href='#'><img src='images/addCircle.png'></a>
                                                    </div>
                                                    <span>Three Red Ninja's</span> Triple Schezwan <img src='images/chilli.png'> <img src='images/chilli.png'><br>
                                                    <p>
                                                        <input class='with-gap' type='radio' id='Veg' name='1' />
                                                        <label for='Veg'>Veg</label>
                                                    </p>
                                                    <p>
                                                        <input class='with-gap' type='radio' id='Chicken'  name='1'/>
                                                        <label for='Chicken'>Chicken</label>
                                                    </p>
                    
                                                </li>-->

                    <?php
                    foreach ($items as $item) {
                        echo "<li id='item$item[id]'>";
                        echo "<form class='form-item'>";
                        echo "<div class='itemPrice'> ";
                        if (!empty($item['inData'])) {
                            for ($i = 0; count($item['inData']) > $i; $i++) {
                                echo "<span class='inPrice inPrice" . $item['inData'][$i]['name'] . "'>" . $item['inData'][$i]['price'] . "</span>";
                                if (count($item['inData']) != ($i + 1)) {
                                    echo "/";
                                }
                            }
                        } else {
                            echo "<span class='inPrice'>".$item['price']."</span>";
                        }
                        echo " <a href='#'><input type='image' src='images/addCircle.png'></a>";
                        echo "</div>";
                        echo "<span class='pname'>$item[name]</span> $item[subTitle] ";
                        if ($item['chillies'] > 0) {
                            echo "<img src='images/c$item[chillies].png' height='25'>";
                        }
                        echo "<div class='clearB'>";

//                                echo "<p>";
//                                echo "    <input class='with-gap' type='radio' id='Veg' name='1' />";
//                                echo "    <label for='Veg'>Veg</label>";
//                                echo "</p>";
//                                echo "<p>";
//                                echo "    <input class='with-gap' type='radio' id='Chicken'  name='1'/>";
//                                echo "    <label for='Chicken'>Chicken</label>";
//                                echo "</p>";
                        for ($i = 0; count($item['inData']) > $i; $i++) {
                            echo "<p>";
                            //$sname = str_replace(" ", "", $item['name']);
                            $inPrice = $item['inData'][$i]['price'];
                            $inName = $item['inData'][$i]['name'];
                            echo "    <input class='with-gap' onClick=selectPrice('#item$item[id]','$inPrice','$inName',this) type='radio' id='" . $item['inData'][$i]['name'] . $item['id'] . "' value='" . $item['inData'][$i]['name'] . "' name='product_ingridient' />";
                            echo "    <label for='" . $item['inData'][$i]['name'] . $item['id'] . "'>" . $item['inData'][$i]['name'] . "</label>";
                            echo "</p>";
                        }
                        echo "</div>";
                        echo "<input type='hidden' value='$item[id]' name='product_code'>";
                        if (empty($item['inData'])) {
                            echo "<input type='hidden' value='$item[price]' class='product_price' name='product_price'>";
                            echo "<input type='hidden' value='default' name='product_ingridient'>";
                        } else {
                            echo "<input type='hidden' value='' class='product_price' name='product_price'>";
                        }
                        echo "<input type='hidden' value='ready' name='product_type'>";
                        echo "<input type='hidden' value='".$menuId."' name='product_menu'>";
                        echo "</form>";
                        echo "</li>";
                    }
                    ?>
                </ul>


            </div>
            <?php include "cart.php"; ?>
        </div>
        <?php include "footer.php"; ?>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery.min.js"></script>

        <script src="js/materialize.min.js"></script>
        <script src="js/common.js"></script>
        <script>
            
            $(document).ready(function () {
                
                window.cartCount = 0;

                $.ajax({//make ajax request to cart_process.php
                    url: "cart_process.php",
                    type: "POST",
                    dataType: "json", //expect json value from server
                    data: "getCart=1"
                }).done(function (data) { //on Ajax success
                    cartDisplay(data);
                    //alert(Object.keys(data.products).length);
                    cartPop(data);
                })
                
                $("#placeOrderForm").submit(function (e) {
                    e.preventDefault();
                    var form_data = $(this).serialize() + "&placeOrder=1";
                    $.ajax({//make ajax request to cart_process.php
                            url: "cart_process.php",
                            type: "POST",
                            dataType: "json", //expect json value from server
                            data: form_data
                        }).done(function (data) { //on Ajax success
                            cartDisplay(data);
                            $(".OrderformCon").html(data.status);
                            setTimeout(function(){ location.reload();  }, 2000);
                        });
                })
                
                $(".form-item").submit(function (e) {
                    var form_data = $(this).serialize() + "&add=1";
                    var button_content = $(this).find('button[type=submit]');
                    button_content.html('Adding...'); //Loading button text 

                    var productPrice = $(this).find(".product_price").val();
                    var productName = $(this).find(".pname").html();

                    if (productPrice !== "") {
                        $.ajax({//make ajax request to cart_process.php
                            url: "cart_process.php",
                            type: "POST",
                            dataType: "json", //expect json value from server
                            data: form_data
                        }).done(function (data) { //on Ajax success
                            cartDisplay(data);
                            cartPop(data);
                            Materialize.toast(productName+' Added!', 3000)
                        });
                        e.preventDefault();
                    } else {
                        //alert("Please select item.");
                        $(this).append("<p class='error'>Please select item.</p>");
                        e.preventDefault();
                    }
                });

                $("#completeWok").click(function (e) {
                    var form_data = "currentWok=1";
                    $.ajax({//make ajax request to cart_process.php
                            url: "cart_process.php",
                            type: "POST",
                            dataType: "json", //expect json value from server
                            data: form_data
                        }).done(function (data) { //on Ajax success
                            cartDisplay(data);
                            cartPop(data);
                        });
                        e.preventDefault();
                });


                $("#cart-info").on('click', 'span.qtyWork', function (e) {
                    e.preventDefault();
                    var pWok = $(this).attr("data-product-work"); //get product code
                    var pQty = $(this).attr("data-product-qty"); //get product code
                    var form_data = "wQty="+pQty+"&product_work="+pWok;
                    $.ajax({//make ajax request to cart_process.php
                            url: "cart_process.php",
                            type: "POST",
                            dataType: "json", //expect json value from server
                            data: form_data
                        }).done(function (data) { //on Ajax success
                            cartDisplay(data);
                            cartPop(data);
                        });
                });   
                
                $("#cart-info").on('click', 'span.qtyUpdate', function (e) {
                    e.preventDefault();
                    var pQty = $(this).attr("data-product-qty"); //get product code
                    var pCode = $(this).attr("data-product-code"); //get product code
                    var pIngridient = $(this).attr("data-product-ingridient"); //get product code
                    var pType = $(this).attr("data-product-type"); //get product code
                    var pStep = $(this).attr("data-product-step"); //get product code

                    //alert(pQty); return false;
                    if (pType == "ready") {
                        var formData = "qty=" + pQty + "&product_code=" + pCode + "&product_ingridient=" + pIngridient + "&product_type=" + pType;

                        $.ajax({//make ajax request to cart_process.php
                            url: "cart_process.php",
                            type: "POST",
                            dataType: "json", //expect json value from server
                            data: formData
                        }).done(function (data) { //on Ajax success
                            cartDisplay(data);
                            //alert(data);
                            cartPop(data);
                        });
                    }

                    if (pType == "wok") {
                        var formData = "product_step=" + pStep + "&qty=" + pQty + "&product_code=" + pCode + "&product_ingridient=" + pIngridient + "&product_type=" + pType;
                        //alert(formData);return false;
                        $.ajax({//make ajax request to cart_process.php
                            url: "cart_process.php",
                            type: "POST",
                            dataType: "json", //expect json value from server
                            data: formData
                        }).done(function (data) { //on Ajax success
                            cartDisplay(data);
                            //alert(data);
                            cartPop(data);
                        });
                    }

                });
                
                $("#checkout").click(function (e) {
                    if (window.cartCount == 1) {
                        $('#orderPop').openModal();
                        //$('#checkout').leanModal();
                    } else {
                        $('#cartEmpty').openModal();
                    }
                })
                
            });
            
            function cartPop(data) {
                if (data.products.total === 0 || data.products.total === "" || data.products === "") {
                    //Object.keys(data.products).length;
                    window.cartCount = 0;
                    
                    if (data.products.currentWok != "") {
                        $(".cartEMessage").html("Please complete your wok.");
                    } else {
                        $(".cartEMessage").html("Your cart is empty.");
                    }
                    
                } else {
                    window.cartCount = 1;
                }
            }
            
            
            function cartDisplay(data) {
                $("#cart-info").html("");
                if (data.products !== undefined) {
                    if (data.products.ready !== undefined) {
                        jQuery.each(data.products.ready, function (index, value) {
                            //console.log('element at index ' + index + ' is ' + value);
                            //alert(value.product_name);
                            $("#cart-info").prepend("<div id='item"+index+"' class='itemCartCon'></div>");
                            
                            $("#cart-info #item"+index).append('<p><strong>' + value.product_name + '</strong></p>');
                            jQuery.each(value.product_ingridient, function (index1, value1) {
                                //htmlData =+ '<p><strong>'+index1+'</strong></p>';
                                if (index1 === "default") {
                                    var subMenuText = "";
                                } else {
                                    var subMenuText = index1 + " :";
                                }
                                $("#cart-info #item"+index).append('<p><span class="btnSM qtyUpdate" data-product-type="ready" data-product-code="' + index + '" data-product-ingridient="' + index1 + '" data-product-qty="remove">-</span> <span class="btnSM qtyUpdate" data-product-type="ready" data-product-code="' + index + '" data-product-ingridient="' + index1 + '" data-product-qty="add">+</span>' + subMenuText + ' Rs. ' + value1.price + ' x ' + value1.qty + '</p>');
                                //console.log('element at index1 ' + index1 + ' is ' + value1);
                                jQuery.each(value1, function (index2, value2) {
                                    //console.log('element at index2 ' + index2 + ' is ' + value2);
                                });
                            });
                        });
                    }

                    if (data.products.wok !== undefined) {


                        jQuery.each(data.products.wok, function (indexW, valueW) {
                            

                            // Current WOK
                            if (data.products.currentWok === indexW) {
                                $("#cart-info").prepend("<div id='"+indexW+"' class='wokCartCon'></div>");
                                $("#cart-info #"+indexW).append("<p class='t"+indexW+" wTitle'></p>");
                                $("#cart-info #"+indexW+" .t"+indexW).html('<strong>' + indexW + ' x '+valueW.qty+'</strong>');
                                
                                jQuery.each(valueW.items, function (index, value) {
                                    //console.log('element at index ' + index + ' is ' + value);
                                    //alert(value.product_name);
                                    var step;
                                    if (value.product_step == 4) {
                                        step = "Extras";
                                    } else {
                                        step = "Step " + value.product_step;
                                    }
                                    $("#cart-info #"+indexW+" ").append("<p class='cstep cstep" + value.product_step + "'></p>");
                                    $("#cart-info #"+indexW+" .cstep" + value.product_step).html('<strong>' + step + '</strong>');
                                    $("#cart-info #"+indexW+" ").append('<p class="cname"><strong>' + value.product_name + '</strong></p>');
                                    jQuery.each(value.product_ingridient, function (index1, value1) {
                                        //htmlData =+ '<p><strong>'+index1+'</strong></p>';
                                        if (index1 === "default") {
                                            var subMenuText = "";
                                        } else {
                                            var subMenuText = index1 + " :";
                                        }
                                        var qtyOpt;
                                        if (value.product_step == 4) {
                                            qtyOpt = '<span class="btnSM qtyUpdate" data-product-step="' + value.product_step + '" data-product-type="wok" data-product-code="' + index + '" data-product-ingridient="' + index1 + '" data-product-qty="remove">-</span> <span class="btnSM qtyUpdate" data-product-step="' + value.product_step + '" data-product-type="wok" data-product-code="' + index + '" data-product-ingridient="' + index1 + '" data-product-qty="add">+</span>';
                                        } else {
                                            qtyOpt = '';
                                        }
                                        $("#cart-info #"+indexW+" ").append('<p>' + qtyOpt + subMenuText + ' Rs. ' + value1.price + ' x ' + value1.qty + '</p>');
                                        //console.log('element at index1 ' + index1 + ' is ' + value1);
                                        jQuery.each(value1, function (index2, value2) {
                                            //console.log('element at index2 ' + index2 + ' is ' + value2);
                                        });
                                    });
                                });
                            } else {
                                $("#cart-info").prepend("<div id='"+indexW+"' class='wokCartCon'></div>");
                                $("#cart-info #"+indexW).append("<p class='t"+indexW+" wTitle'></p>");
                                $("#cart-info #"+indexW+"  .t"+indexW).html('<strong>' + indexW + ' x '+valueW.qty+'</strong><span class="btnSM qtyWork" data-product-work="' + indexW + '" data-product-qty="add">+</span><span class="btnSM qtyWork" data-product-work="' + indexW + '" data-product-qty="remove">-</span>');
                                
                                jQuery.each(valueW.items, function (index, value) {
                                    //console.log('element at index ' + index + ' is ' + value);
                                    //alert(value.product_name);
                                    var step;
                                    if (value.product_step == 4) {
                                        step = "Extras";
                                    } else {
                                        step = "Step " + value.product_step;
                                    }
                                    $("#cart-info #"+indexW+" ").append("<p class='cstep cstep" + value.product_step + "'></p>");
                                    $("#cart-info #"+indexW+" .cstep" + value.product_step).html('<strong>' + step + '</strong>');
                                    $("#cart-info #"+indexW+" ").append('<p class="cname"><strong>' + value.product_name + '</strong></p>');
                                    jQuery.each(value.product_ingridient, function (index1, value1) {
                                        //htmlData =+ '<p><strong>'+index1+'</strong></p>';
                                        if (index1 === "default") {
                                            var subMenuText = "";
                                        } else {
                                            var subMenuText = index1 + " :";
                                        }
                                        
                                        $("#cart-info #"+indexW+" ").append('<p>' + subMenuText + ' Rs. ' + value1.price + ' x ' + value1.qty + '</p>');
                                        //console.log('element at index1 ' + index1 + ' is ' + value1);
                                        jQuery.each(value1, function (index2, value2) {
                                            //console.log('element at index2 ' + index2 + ' is ' + value2);
                                        });
                                    });
                                });
                            }

                        });



                    }

                    $(".total").html('Total : Rs. <span>' + data.products.total + '</span>');

                    if (data.products.total === 0 || data.products.total === "" || data.products === "") {
                        $("#cart-info").html('<p>Your cart is empty.</p>');
                         $(".total").html("");
                    }
                } else {
                    $("#cart-info").html('<p>Your cart is empty.</p>');
                    $(".total").html("");
                }
            }

            function selectPrice(element, price, name, current) {
                $(".inPrice").removeClass("selectPrice");
                $(".product_price").val("");

                $(element + " .inPrice" + name).addClass("selectPrice");
                $("input[type=radio]").prop('checked', false);
                $(current).prop('checked', true);

                $(element + " .product_price").val(price);

                $(".error").remove();
            }
            
        </script>
    </body>
</html>