<?php 
session_start(); //start session
include_once 'db.php';

$menus = $db->get('tbl_resto_menu');

//$queryMenus = "SELECT * FROM tbl_resto_menu";
//$resultMenus = $db->query($queryMenus);
//while ($rowmenus = $resultMenus->fetch_assoc()) {
//    $menus[] = $rowmenus;
//}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title></title>

        <link href="css/style.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="menuBg">
        </div>    
            <?php include "header.php"; ?>

            <div class="foodMenu">
                <?php
                    $cnt = 0;
                    foreach ($menus as $menu) { 
                       $cnt++;
                ?>
                       <div id="item<?php echo $cnt; ?>" class="foodMenuItem <?php echo str_replace(" ", "", $menu['name']); ?>">
                           <a href="items.php?menuId=<?php echo $menu['id']; ?>">
                               <img src="images/<?php echo str_replace(" ", "", $menu['name']); ?>.png" />

                               <h3>
                                   <?php echo $menu['name']; ?>
                               </h3>
                           </a>
                       </div>
                <?php
                    }
                ?>
                
            </div>
            <?php include "footer.php"; ?>
        

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery.min.js"></script>
        <script src="js/TweenMax.min.js"></script>
        <!-- <script src="js/ScrollMagic.min.js"></script>
        <script src="js/animation.gsap.min.js"></script>
        <script src="js/debug.addIndicators.min.js"></script>
        <script src="js/jquery.jparallax.min.js"></script> -->
        <script type="text/javascript" src="js/plax.js"></script>
        <script src="js/common.js"></script>
        <script>
            $(function () { // wait for document ready

                // Mountains Movement
                //jQuery('#item1').parallax({xparallax: "30px", yparallax: "0px"});
                //jQuery('#item2').parallax({xparallax: "30px", yparallax: "0px"});
                //jQuery('.foodMenuItem').parallax({xparallax: "20px", yparallax: 0});

                var item1 = $("#item1 img"), item2 = $("#item2 img"), item3 = $("#item3 img"), item4 = $("#item4 img"), item5 = $("#item5 img"), item6 = $("#item6 img");

                var title1 = $("#item1 h3"), title2 = $("#item2 h3"), title3 = $("#item3 h3"), title4 = $("#item4 h3"), title5 = $("#item5 h3"), title6 = $("#item6 h3");
                var items = [item1, item2, item3, item4, item5, item6];

                var titles = [title1, title2, title3, title4, title5, title6];

                TweenMax.staggerFromTo(items, 0.5, {opacity: 0, scale: 0.5}, {opacity: 1, scale: 1, ease: Back.easeInOut}, 0.25);

                TweenMax.staggerFromTo(titles, 0.5, {opacity: 0, scale: 0.5}, {opacity: 1, scale: 1, ease: Strong.easeOut}, 0.25);



                $('#item1 img').plaxify({"xRange": 20, "yRange": 20, "invert": true})
                $('#item2 img').plaxify({"xRange": 20, "yRange": 20, "invert": true})
                $('#item3 img').plaxify({"xRange": 20, "yRange": 20, "invert": true})
                $('#item4 img').plaxify({"xRange": 20, "yRange": 20, "invert": true})
                $('#item5 img').plaxify({"xRange": 20, "yRange": 20, "invert": true})
                $('#item6 img').plaxify({"xRange": 20, "yRange": 20, "invert": true})
                $.plax.enable()



            });
        </script>
    </body>
</html>