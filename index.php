<?php 
session_start(); //start session
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title></title>

        <link href="css/style.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div id="pageContainer">

            <?php include "header.php"; ?>

            <section id="stepOne" class="panel stepOne">
                <div class="kitli"><img class="layer1" src="images/kitli.png"></div>   

                <div class="mountains"><img class="layer" src="images/mountains.png"></div>  
                <img class="birds" src="images/bird.gif">   
                <img class="scrollImg" src="images/scroll.png">   
                <img class="textOne objOne" src="images/textOne.png">
                <div class="cookingCon">
                    <img class="silli objOne" src="images/silli.png">            
                    <img class="chilli objOne" src="images/chilli.png">            
                    <img class="kadhai objOne" src="images/kadhai.png">            
                    <img class="ingridients objOne" src="images/ingridients.png">
                </div>         
            </section>
            <section class="panel stepTwo">
                <div class="mountainsTwo"><img class="" src="images/mountainsTwo.png"></div>
                <img class="sun" src="images/sun.png">
                <img class="textTwo" src="images/textTwo.png">
                <img class="camel" src="images/camel.png">
            </section>
            <section class="panel stepThree">
                <img class="lanternOne stag" src="images/lanternOne.png">
                <img class="lanternTwo stag" src="images/lanternTwo.png">
                <img class="lanternThree stag" src="images/lanternThree.png">
                <img class="lanternFour stag" src="images/lanternFour.png">
                <img class="window" src="images/window.jpg">
                <img class="table stag" src="images/table.png">
                <img class="frame stag" src="images/frame.png">
                <img class="textThree stag" src="images/textThree.png">
                <img class="framePicOne stag" src="images/framePicOne.png">
                <img class="framePicTwo stag" src="images/framePicTwo.png">
                <img class="framePicThree stag" src="images/framePicThree.png">

            </section>
            <section class="panel stepFour">
                <img class="textFour" src="images/textFour.png">
                <img class="bridge" src="images/bridge.png">
                <img class="shops" src="images/shops.png">
                <img class="peoples" src="images/peoples.png">
                <img class="track" src="images/track.png">
                <img class="tram" src="images/tram.png">
            </section>
            <section class="panel stepFive">
                <img class="textFive" src="images/textFive.png">
					<div class="grayDiv">
						<img class="base" src="images/base.png">
						<img class="graySilli" src="images/graySilli.png">
						<img class="roll" src="images/roll.png">
						<img class="grayChilli" src="images/grayChilli.png">
					</div>
                <img class="greenMan" src="images/greenMan.png">
                <img class="redMan" src="images/redMan.png">
            </section>
            <section class="panel stepSix">
                <img class="address" src="images/address.png">
                <img class="mainShop" src="images/mainShop.png">
                <img class="greenSilli" src="images/greenSilli.png">
                <img class="redChilli" src="images/redChilli.png">

            </section>
            <?php include "footer.php"; ?>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery.min.js"></script>
        <script src="js/TweenMax.min.js"></script>
        <script src="js/ScrollMagic.min.js"></script>
        <script src="js/animation.gsap.min.js"></script>
        <script src="js/debug.addIndicators.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/plugins/ScrollToPlugin.min.js"></script>
        <script src="js/jquery.jparallax.min.js"></script>
        <script src="js/common.js"></script>
        
        <script>
            $(function () { // wait for document ready

                function smoothScroll() {
                    var a = $(window);
                    a.on("mousewheel DOMMouseScroll", function (b) {
                        b.preventDefault();
                        b = b.originalEvent.wheelDelta / 120 || -b.originalEvent.detail / 3;
                        b = a.scrollTop() - parseInt(280 * b);
                        TweenMax.to(a, 1.1, {scrollTo: {y: b, autoKill: !0}, ease: Power1.easeOut, overwrite: 5})
                    })
                }

                smoothScroll();

                // Mountains Movement
                jQuery('.layer').parallax({xparallax: "30px", yparallax: 0});
                jQuery('.layer1').parallax({xparallax: "20px", yparallax: 0});

                // Birds Animation
                var birds = $('.birds');
                TweenMax.to(birds, 22, {right: "100%", repeat: -1});

                // init
                var controller = new ScrollMagic.Controller();

                function windowZoom() {
                    TweenMax.fromTo($('.lanternOne'), 1, {opacity: "1"}, {opacity: "0", ease: Circ.easeNone});
                }

                // define movement of panels
                var wipeAnimation = new TimelineMax()
                        // .delay(10)
                        // Step One animation
                        .fromTo(".scrollImg", 15, {opacity: "1"}, {opacity: "0", ease: Circ.easeInOut})
                        .fromTo(".textOne", 200, {x: "200px"}, {x: "-300px", ease: Back.easeOut})
                        .fromTo(".silli", 210, {x: "-100px", opacity: "0"}, {x: "0", opacity: "1", ease: Circ.easeInOut})
                        .fromTo(".chilli", 210, {x: "100px", opacity: "0"}, {x: "0", opacity: "1", ease: Circ.easeInOut})
                        .fromTo(".kadhai", 210, {opacity: "0"}, {opacity: "1", ease: Circ.easeInOut})
                        .fromTo(".ingridients", 210, {y: "-30px", opacity: "0"}, {y: "0", opacity: "1", ease: Circ.easeInOut})
                        //.delay(10)
                        // Step Two animation
                        .fromTo("section.panel.stepTwo", 250, {opacity: "0"}, {opacity: "1", delay: "355", ease: Linear.easeNone})
                        .fromTo(".sun", 250, {y: "100px", opacity: "0"}, {y: "0", opacity: "1", delay: "35", ease: Circ.easeInOut})
                        .fromTo(".textTwo", 250, {x: "330px"}, {x: "-300px", delay: "135", ease: Back.easeOut})
                        .fromTo(".camel", 550, {x: "-150%"}, {x: "0%", delay: "35", ease: Back.easeOut})

                        // Step Three animation
                        .fromTo("section.panel.stepThree", 150, {opacity: "0"}, {opacity: "1", delay: "1585", ease: Linear.easeNone})
                        .fromTo(".lanternOne", 200, {y: "-100%"}, {y: "0%", delay: "585", ease: Bounce.easeOut})
                        .fromTo(".lanternTwo", 200, {y: "-100%"}, {y: "0%", ease: Bounce.easeOut})
                        .fromTo(".lanternThree", 200, {y: "-100%"}, {y: "0%", ease: Bounce.easeOut})
                        .fromTo(".lanternFour", 200, {y: "-100%"}, {y: "0%", ease: Bounce.easeOut})
                        .fromTo(".window", 250, {opacity: "0"}, {opacity: "1", delay: "585", ease: Circ.easeNone})
                        .fromTo(".table", 250, {opacity: "0"}, {opacity: "1", ease: Circ.easeNone})
                        .fromTo(".frame", 250, {opacity: "0"}, {opacity: "1", delay: "585", ease: Circ.easeNone})
                        .fromTo(".textThree", 200, {x: "-150%", opacity: "0"}, {x: "0%", opacity: "1", ease: Elastic.easeOut})
                        .fromTo(".framePicOne", 200, {opacity: "0"}, {opacity: "1", delay: "550", ease: Circ.easeInOut})
                        .fromTo(".framePicTwo", 200, {opacity: "0"}, {opacity: "1", delay: "550", ease: Circ.easeInOut})
                        .fromTo(".framePicThree", 200, {opacity: "0"}, {opacity: "1", delay: "550", ease: Circ.easeInOut})
                        .from(".window", 150, {width: "280px"})
                        .staggerTo(".stag", 150, {opacity: "0", x: "-100px", delay: "585"}, 0.5)
                        .to(".window", 150, {width: "750px", x: "-150px", ease: SlowMo.easeIn})
                        .to(".window", 150, {width: "1050px", opacity: "0", x: "0", y: "-150px", ease: SlowMo.easeIn})

                        // Step Four animation
                        .fromTo("section.panel.stepFour", 150, {opacity: "0"}, {opacity: "1", delay: "-50", ease: Linear.easeNone})
                        .fromTo(".textFour", 250, {opacity: "0", x: "-100%"}, {opacity: "1", x: "0%", ease: Back.easeOut})
                        .fromTo(".bridge", 250, {opacity: "0"}, {opacity: "0.5", ease: Circ.easeNone})
                        .fromTo(".shops", 200, {opacity: "0", scale: 0.1}, {opacity: "1", scale: 1, ease: Back.easeInOut})
                        .fromTo(".peoples", 250, {opacity: "0"}, {opacity: "1", ease: Circ.easeInOut})
                        .fromTo(".track", 250, {opacity: "0"}, {opacity: "1", ease: Circ.easeInOut})
                        .to(".tram", 2500, {left: "100%", ease: Circ.easeNone})

                        // Step Five animation
                        .fromTo("section.panel.stepFive", 250, {opacity: "0"}, {opacity: "1", delay: "150", ease: Linear.easeNone})
                        .fromTo(".textFive", 200, {x: "-150%", opacity: "0"}, {x: "0%", opacity: "1", delay: "150", ease: Elastic.easeOut})
                        .fromTo(".base", 250, {opacity: "0", scale: 0.1}, {opacity: "1", scale: 1, ease: Back.easeInOut})
                        .fromTo(".graySilli", 250, {opacity: "0", x: "-100px"}, {opacity: "1", x: "0", ease: Circ.easeNone})
                        .fromTo(".grayChilli", 250, {opacity: "0", x: "100px"}, {opacity: "1", x: "0", ease: Circ.easeNone})
                        .fromTo(".roll", 150, {opacity: "0", y: "-100px"}, {opacity: "1", y: "0", ease: Circ.easeNone})
                        .fromTo(".greenMan", 250, {opacity: "0", x: "-100px"}, {opacity: "1", x: "0", ease: Circ.easeNone})
                        .fromTo(".redMan", 250, {opacity: "0", x: "100px"}, {opacity: "1", x: "0", ease: Circ.easeNone})

                        // Step Six animation
                        .fromTo("section.panel.stepSix", 500, {opacity: "0"}, {opacity: "1", delay: "150", ease: Linear.easeNone})
                        .fromTo(".address", 200, {x: "-150%", opacity: "0"}, {x: "0%", opacity: "1", delay: "150", ease: Elastic.easeOut})
                        .fromTo(".greenSilli", 250, {opacity: "0", x: "-100px"}, {opacity: "1", x: "0", ease: Circ.easeNone})
                        .fromTo(".redChilli", 250, {opacity: "0", x: "100px"}, {opacity: "1", x: "0", ease: Circ.easeNone})
                        .fromTo(".mainShop", 250, {opacity: "0", scale: 0.1}, {opacity: "1", scale: 1, ease: Back.easeInOut})
                        ;
                //.fromTo("section.panel.bordeaux", 10, {y: "-100%"}, {y: "0%", delay: "50", ease: Linear.easeNone}); // in from top

                // create scene to pin and link animation
                var scene = new ScrollMagic.Scene({
                    triggerElement: "#pageContainer",
                    triggerHook: "onLeave",
                    duration: "800%"
                })
                        .setPin("#pageContainer")
                        .setTween(wipeAnimation)
                        // .addIndicators() // add indicators (requires plugin)
                        .addTo(controller);




            });
        </script>
    </body>
</html>