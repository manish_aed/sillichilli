<footer class="footCon">
    <div class="socialLinks">
        <a class="" href="#"><img src="images/tw.png"></a>
        <a class="" href="#"><img src="images/fb.png"></a>
        <a class="" href="#"><img src="images/zomato.png"></a>
        <a class="" href="#"><img src="images/insta.png"></a>
    </div>
    <div class="footInfo">
        Bandra (W)                        022  26003666 | 65676666<br>
        Andheri Lokhandwala   022 26336666 | +91 8097766667 
        <img class="callImg" src="images/callIcon.png"> 
    </div>
    <span class="footInfoTxt"><a href="mailto:feedback@sillichilli.in">feedback@sillichilli.in</a> ©2015 SilliChilli. Created by <a href="http://www.aed.in">www.aed.in </a> </span>
</footer>
<!-- Modal Structure -->
<div id="orderPop" class="modal modal-fixed-footer">
    <form id="placeOrderForm">
        <div class="modal-content">
            <h4>Your Cart</h4>


            <div class="row OrderformCon">

                <div class="row">
                    <div class="input-field col s6">
                        <input id="first_name" name="first_name" type="text" class="validate" required="">
                        <label for="first_name">First Name</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="last_name" name="last_name" type="text" class="validate" required="">
                        <label for="last_name">Last Name</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="email" name="email" type="email" class="validate" required="">
                        <label for="email">Email</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="phone" name="phone" type="text" class="validate" required="">
                        <label for="phone">Phone</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <textarea id="address" name="address" class="materialize-textarea" required=""></textarea>
                        <label for="adrress">Address</label>
                    </div>
                </div>

            </div>



        </div>
        <div class="modal-footer">
            <span class="modal-action modal-close waves-effect waves-green btn-flat green">Close</span>
            <button id="placeOrder" class="waves-effect waves-green btn-flat red" style="color:white">Place Order</button>
        </div>
    </form>    
</div>

<div id="cartEmpty" class="modal modal-fixed-footer">
    
        <div class="modal-content">
            <h4>Your Cart</h4>


            <p class="cartEMessage">Your cart is empty.</p>



        </div>
        <div class="modal-footer">
            <span class="modal-action modal-close waves-effect waves-green btn-flat green">Close</span>
        </div>
     
</div>