-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 10, 2015 at 02:23 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sillichilli`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart_items`
--

CREATE TABLE IF NOT EXISTS `cart_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderId` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `productName` varchar(255) NOT NULL,
  `menuName` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderId` (`orderId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=108 ;

--
-- Dumping data for table `cart_items`
--

INSERT INTO `cart_items` (`id`, `orderId`, `type`, `productName`, `menuName`, `qty`) VALUES
(76, 54, 'ready', 'Electric Thai Bhel', 'Starters', 1),
(77, 54, 'ready', 'Silli Chilli', 'Starters', 1),
(78, 54, 'ready', 'Dragon Spring Rolls', 'Starters', 1),
(79, 54, 'ready', 'Kung Pao Panda ', 'Starters', 1),
(80, 55, 'ready', 'Manchurian Rice', 'All Time Favourites', 1),
(81, 55, 'ready', 'Manchurian Noodles', 'All Time Favourites', 1),
(82, 55, 'wok', 'Wok-Paneer1', '', 2),
(101, 57, 'ready', 'Three Red Ninjas', 'All Time Favourites', 1),
(102, 58, 'ready', 'Three Red Ninjas', 'All Time Favourites', 1),
(103, 59, 'ready', 'Three Red Ninjas', 'All Time Favourites', 1),
(104, 60, 'ready', 'Three Red Ninjas', 'All Time Favourites', 1),
(105, 61, 'ready', 'Three Red Ninjas', 'All Time Favourites', 1),
(106, 62, 'ready', 'Three Red Ninjas', 'All Time Favourites', 1),
(107, 63, 'ready', 'Three Red Ninjas', 'All Time Favourites', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cart_item_ingridients`
--

CREATE TABLE IF NOT EXISTS `cart_item_ingridients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productIngridient` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `step` int(11) NOT NULL,
  `cartItemsId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cartItemsId` (`cartItemsId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=105 ;

--
-- Dumping data for table `cart_item_ingridients`
--

INSERT INTO `cart_item_ingridients` (`id`, `productIngridient`, `qty`, `price`, `step`, `cartItemsId`) VALUES
(67, 'default', 1, 150, 0, 76),
(68, 'Potatoes', 1, 170, 0, 77),
(69, 'Lamb', 3, 270, 0, 77),
(70, 'Chicken', 1, 200, 0, 78),
(71, 'Paneer', 2, 190, 0, 79),
(72, 'Chicken', 1, 190, 0, 80),
(73, 'Chicken', 1, 190, 0, 81),
(74, 'Paneer', 1, 180, 0, 81),
(75, 'Paneer', 1, 180, 1, 82),
(76, 'Hakka Noodles', 1, 0, 2, 82),
(77, 'Black Pepper', 1, 0, 3, 82),
(78, 'Sprouts', 1, 20, 4, 82),
(79, 'Babycorn', 1, 20, 4, 82),
(98, 'Chicken', 1, 190, 0, 101),
(99, 'Chicken', 1, 190, 0, 102),
(100, 'Chicken', 1, 190, 0, 103),
(101, 'Chicken', 1, 190, 0, 104),
(102, 'Chicken', 1, 190, 0, 105),
(103, 'Chicken', 1, 190, 0, 106),
(104, 'Chicken', 1, 190, 0, 107);

-- --------------------------------------------------------

--
-- Table structure for table `learn_chinese`
--

CREATE TABLE IF NOT EXISTS `learn_chinese` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quotes` varchar(255) NOT NULL,
  `orderBy` int(11) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `learn_chinese`
--

INSERT INTO `learn_chinese` (`id`, `quotes`, `orderBy`, `created`) VALUES
(1, 'That''s not right.................................Sum Ting Wong', 0, '2015-11-10 08:18:32'),
(2, 'Are you harbouring a fugitive..........Hu Yu Hai Ding', 0, '2015-11-10 08:18:32'),
(3, 'See me ASAP.....................................................Kum Hia', 0, '2015-11-10 08:19:24'),
(4, 'Stupid Man............................................................Dum Fuk', 0, '2015-11-10 08:19:24');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `link` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` longtext NOT NULL,
  `total` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `firstName`, `lastName`, `email`, `phone`, `address`, `total`, `created`) VALUES
(54, 'man', 'man', 'man@man.ccc', '2324234', 'asdsa dsad sa', 1710, '2015-10-30 11:10:48'),
(55, 'man', 'man', 'man@man.ccc', '2324234', 'sdsa sadsad', 1000, '2015-10-30 11:59:19'),
(56, 'man', 'man', 'man@man.ccc', '2324234', 'a', 190, '2015-11-04 11:42:05'),
(57, 'man', 'man', 'man@man.ccc', '2324234', 'a', 190, '2015-11-04 11:43:31'),
(58, 'man', 'man', 'man@man.ccc', '2324234', 'a', 190, '2015-11-04 11:44:31'),
(59, 'man', 'man', 'man@man.ccc', '2324234', 'a', 190, '2015-11-04 11:45:35'),
(60, 'man', 'man', 'man@man.ccc', '2324234', 'a', 190, '2015-11-04 11:46:16'),
(61, 'man', 'man', 'man@man.ccc', '2324234', 'saddsa', 190, '2015-11-04 11:47:00'),
(62, 'man', 'man', 'man@man.ccc', '2324234', 'a', 190, '2015-11-04 11:48:09'),
(63, 'man', 'man', 'man@man.ccc', '2324234', 'a', 190, '2015-11-04 12:03:08');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_resto_ingredient`
--

CREATE TABLE IF NOT EXISTS `tbl_resto_ingredient` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(4000) NOT NULL,
  `inOrder` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_resto_ingredient`
--

INSERT INTO `tbl_resto_ingredient` (`id`, `name`, `inOrder`) VALUES
(1, 'Veg', 1),
(2, 'Chicken', 3),
(3, 'Prawns', 5),
(4, 'Paneer', 2),
(5, 'Fish', 4),
(6, 'Lamb', 6),
(7, '1 litre', 1),
(8, '500 ml', 2),
(9, 'Potatoes', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_resto_item`
--

CREATE TABLE IF NOT EXISTS `tbl_resto_item` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(4000) NOT NULL,
  `subTitle` varchar(255) DEFAULT NULL,
  `chillies` int(11) DEFAULT '0',
  `price` int(12) DEFAULT NULL,
  `menuId` int(10) NOT NULL,
  `step` varchar(255) DEFAULT NULL,
  `type` varchar(1000) DEFAULT NULL,
  `new_item` tinyint(1) DEFAULT NULL,
  `unit` varchar(1000) DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `menuId` (`menuId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `tbl_resto_item`
--

INSERT INTO `tbl_resto_item` (`id`, `name`, `subTitle`, `chillies`, `price`, `menuId`, `step`, `type`, `new_item`, `unit`, `quantity`, `description`) VALUES
(1, 'Three Red Ninjas', 'Triple Schezwan', 2, NULL, 2, '', '', NULL, '', NULL, ''),
(2, 'All American Chopsuey', '', NULL, NULL, 2, '', '', NULL, '', NULL, ''),
(3, 'Electric Thai Bhel', '', 2, 150, 1, '', '', NULL, '', NULL, ''),
(4, 'Veg', '', NULL, 150, 6, '1', '', NULL, '', NULL, ''),
(5, 'Paneer', '', NULL, 180, 6, '1', '', NULL, '', NULL, ''),
(6, 'Chicken', '', NULL, 180, 6, '1', '', NULL, '', NULL, ''),
(7, 'Fish', '', NULL, 220, 6, '1', '', NULL, '', NULL, ''),
(8, 'Prawns', '', NULL, 250, 6, '1', '', NULL, '', NULL, ''),
(9, 'Lamb', '', NULL, 250, 6, '1', '', NULL, '', NULL, ''),
(10, 'Manchurian Rice', '', NULL, NULL, 2, '', '', NULL, '', NULL, ''),
(11, 'Manchurian Noodles', '', NULL, NULL, 2, '', '', NULL, '', NULL, ''),
(12, 'Silli Chilli Rice', '', 1, NULL, 2, '', '', NULL, '', NULL, ''),
(13, 'Silli Chilli Noodles', '', 1, NULL, 2, '', '', NULL, '', NULL, ''),
(14, 'Bad Boy Chopper Rice', '', 1, NULL, 2, '', '', NULL, '', NULL, ''),
(15, 'High On Thai ', '- Red / Green', NULL, NULL, 2, '', '', NULL, '', NULL, ''),
(16, 'Pan Fried Noodles', 'Ginger Scallion Sauce', NULL, NULL, 2, '', '', NULL, '', NULL, ''),
(17, 'Homemade Gooey Chocolate Brownie ', '', NULL, 50, 5, '', '', NULL, '', NULL, ''),
(18, 'Chocolate Sin Mousse', '', NULL, 60, 5, '', '', NULL, '', NULL, ''),
(19, 'Salted Toffee Caramel', '', NULL, 70, 5, '', '', NULL, '', NULL, ''),
(20, 'Fortune Cookie', '', NULL, 25, 5, '', '', NULL, '', NULL, ''),
(21, 'Water', '', NULL, NULL, 4, '', '', NULL, '', NULL, ''),
(22, 'Aerated Drinks', '', NULL, NULL, 4, '', '', NULL, '', NULL, ''),
(23, 'Diet Coke Can', '', NULL, 50, 4, '', '', NULL, '', NULL, ''),
(24, 'Silli Chilli', '', 1, NULL, 1, '', '', NULL, '', NULL, ''),
(25, 'Spring Chicken Lollipops', '', 1, 200, 1, '', '', NULL, '', NULL, ''),
(26, 'Dragon Spring Rolls', '', 1, NULL, 1, '', '', NULL, '', NULL, ''),
(27, 'Kung Pao Panda ', '', NULL, NULL, 1, '', '', NULL, '', NULL, ''),
(28, 'Fried Rice', '', NULL, 0, 6, '2', '', NULL, '', NULL, ''),
(29, 'Hakka Noodles', '', NULL, 0, 6, '2', '', NULL, '', NULL, ''),
(30, 'Aromatic Rice', '', NULL, 20, 6, '2', '', NULL, '', NULL, ''),
(31, 'Healthy Flat Wheat Noodles', '', NULL, 20, 6, '2', '', NULL, '', NULL, ''),
(32, 'Hunan', '', 1, 0, 6, '3', '', NULL, '', NULL, ''),
(33, 'Chilli Basil', '', 1, 0, 6, '3', '', NULL, '', NULL, ''),
(34, 'Ginger Scallion', '', NULL, 0, 6, '3', '', NULL, '', NULL, ''),
(35, ' Sweet Chilli', '', 1, 0, 6, '3', '', NULL, '', NULL, ''),
(36, 'Teriyaki', '', NULL, 0, 6, '3', '', NULL, '', NULL, ''),
(37, 'Schezwan', '', 2, 0, 6, '3', '', NULL, '', NULL, ''),
(38, 'Kung Pao', '', 1, 0, 6, '3', '', NULL, '', NULL, ''),
(39, 'Black Pepper', '', 2, 0, 6, '3', '', NULL, '', NULL, ''),
(40, 'Black Bean & Soy', '', NULL, 0, 6, '3', '', NULL, '', NULL, ''),
(41, 'Chilli Oyster', '', 1, 0, 6, '3', '', NULL, '', NULL, ''),
(42, 'Sillichilli Dynamite', '', 3, 0, 6, '3', '', NULL, '', NULL, ''),
(43, 'Cantonese', '', NULL, 0, 6, '3', '', NULL, '', NULL, ''),
(44, 'Sweet Silli Garlic', '', NULL, 0, 6, '3', '', NULL, '', NULL, ''),
(45, 'Veg Balls', '', NULL, 30, 6, '4', '', NULL, '', NULL, ''),
(46, 'Egg Fried', '', NULL, 30, 6, '4', '', NULL, '', NULL, ''),
(47, 'Egg Scrambled', '', NULL, 30, 6, '4', '', NULL, '', NULL, ''),
(48, 'Paneer', '', NULL, 40, 6, '4', '', NULL, '', NULL, ''),
(49, 'Chicken', '', NULL, 40, 6, '4', '', NULL, '', NULL, ''),
(50, 'Fish', '', NULL, 60, 6, '4', '', NULL, '', NULL, ''),
(51, 'Prawns', '', NULL, 80, 6, '4', '', NULL, '', NULL, ''),
(52, 'Lamb', '', NULL, 80, 6, '4', '', NULL, '', NULL, ''),
(53, 'Babycorn', '', NULL, 20, 6, '4', '', NULL, '', NULL, ''),
(54, 'Spinach', '', NULL, 20, 6, '4', '', NULL, '', NULL, ''),
(55, 'Sprouts', '', NULL, 20, 6, '4', '', NULL, '', NULL, ''),
(56, 'Onions', '', NULL, 20, 6, '4', '', NULL, '', NULL, ''),
(57, 'Bokchoy', '', NULL, 30, 6, '4', '', NULL, '', NULL, ''),
(58, 'Water Chestnut', '', NULL, 30, 6, '4', '', NULL, '', NULL, ''),
(59, 'Mushroom', '', NULL, 30, 6, '4', '', NULL, '', NULL, ''),
(60, 'Broccoli', '', NULL, 30, 6, '4', '', NULL, '', NULL, ''),
(61, 'Bell Pepper', '(Red, Yellow, Green)', NULL, 30, 6, '4', '', NULL, '', NULL, ''),
(62, 'Bamboo Shoots', '', NULL, 30, 6, '4', '', NULL, '', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_resto_itemtoingrdient`
--

CREATE TABLE IF NOT EXISTS `tbl_resto_itemtoingrdient` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `itemId` int(10) NOT NULL,
  `ingredientId` int(10) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_resto_itemid_ibfk_1` (`itemId`),
  KEY `tbl_resto_ingredientid_ibfk_1` (`ingredientId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `tbl_resto_itemtoingrdient`
--

INSERT INTO `tbl_resto_itemtoingrdient` (`id`, `itemId`, `ingredientId`, `price`) VALUES
(1, 1, 1, 160),
(2, 1, 2, 190),
(3, 2, 1, 160),
(4, 2, 2, 190),
(5, 2, 3, 270),
(6, 10, 1, 160),
(7, 10, 4, 180),
(8, 10, 2, 190),
(9, 11, 1, 160),
(10, 11, 4, 180),
(11, 11, 2, 190),
(12, 12, 1, 160),
(13, 12, 4, 180),
(14, 12, 2, 190),
(15, 12, 5, 240),
(16, 12, 3, 270),
(17, 12, 6, 270),
(18, 13, 1, 160),
(19, 13, 4, 180),
(20, 13, 2, 190),
(21, 13, 5, 240),
(22, 13, 3, 270),
(23, 13, 6, 270),
(24, 14, 2, 190),
(25, 15, 1, 200),
(26, 15, 2, 240),
(27, 15, 3, 270),
(28, 16, 1, 160),
(29, 16, 2, 190),
(30, 16, 3, 270),
(31, 21, 7, 30),
(32, 21, 8, 15),
(33, 22, 8, 50),
(34, 24, 9, 170),
(35, 24, 4, 190),
(36, 24, 2, 200),
(37, 24, 6, 270),
(38, 26, 1, 170),
(39, 26, 2, 200),
(40, 27, 9, 170),
(41, 27, 4, 190),
(42, 27, 2, 200),
(43, 27, 5, 240);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_resto_menu`
--

CREATE TABLE IF NOT EXISTS `tbl_resto_menu` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_resto_menu`
--

INSERT INTO `tbl_resto_menu` (`id`, `name`, `type`) VALUES
(1, 'Starters', 'vegies'),
(2, 'All Time Favourites', 'vegy'),
(3, 'Dim Sums', 'snacks'),
(4, 'Fizzle Drizzle', 'cold drinks'),
(5, 'Desserts', 'dessert'),
(6, 'Make Your Own Wok', 'custom');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart_items`
--
ALTER TABLE `cart_items`
  ADD CONSTRAINT `orders_con` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`);

--
-- Constraints for table `cart_item_ingridients`
--
ALTER TABLE `cart_item_ingridients`
  ADD CONSTRAINT `cart_item_ingridients_con` FOREIGN KEY (`cartItemsId`) REFERENCES `cart_items` (`id`);

--
-- Constraints for table `tbl_resto_item`
--
ALTER TABLE `tbl_resto_item`
  ADD CONSTRAINT `tbl_resto_item_ibfk_1` FOREIGN KEY (`menuId`) REFERENCES `tbl_resto_menu` (`id`);

--
-- Constraints for table `tbl_resto_itemtoingrdient`
--
ALTER TABLE `tbl_resto_itemtoingrdient`
  ADD CONSTRAINT `tbl_resto_itemtoingrdient_ibfk_1` FOREIGN KEY (`itemId`) REFERENCES `tbl_resto_item` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
